# Srovnání různých přístupů k AI pro hraní piškvorek 

Report je v souboru report.pdf.

## Zadání

Mým cílem je naimplementovat a srovnat klasické a deep learning postupy pro vytvoření AI hrajícího piškvorky.

Piškvorky jsou hra pro dva hráče, kteří se střídají v umisťování kamenů na čtverečkovanou desku. Vyhrává hráč, který jako první vytvoří posloupnost 5 vlastních kamenů v řadě za sebou. Existuje velké množství různých variant piškvorek. Pro tuto práci jsem zvolil variantu s omezenou velikostí desky na 15x15 a s pravidlem, že posloupnost více jak 5 kamenů není výherní.

Pro srovnání jednotlivých AI jsem vybral následující přístupy:
- Poziční AI - vlastní algoritmus hodnotící kvalitu pozice podle lokálních vzorů
- MinMax AI - doplnění Poziční AI o MinMax prohledávání stavového prostoru
- Neuronová síť s klasickým učením
- Neuronová síť s reinforcement learningem

Vyhodnocení provedu svým subjektivním zhodnocením zkušeností s danou AI a také turnajem, který ukáže kvalitu AI při vzájemném hraní proti sobě.

## Pokyny pro spuštění

Program je napsaný v Kotlinu, pro spuštění je nejjednodušší nainstalovat si Intellij IDEA (stačí community) a otevřít adresář jako Gradle projekt. 
V horním menu je pak záložka Run. Případně je možné program spustit ze souboru src/main/kotlin/com/filipdolnik/mvi/main.kt kliknutím na zelenou šipku vedle main funkce.
Pro správné načtení neuronových sítí může být potřeba explicitně nastavit working directory na top level adresář projektu.

V menu se naviguje zadáním čísla příslušné akce a stisknutím enteru.