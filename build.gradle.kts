import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.10"

    application
}

group = "com.filipdolnik"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.4.2")

    implementation("org.deeplearning4j:deeplearning4j-core:1.0.0-beta7")
    implementation("org.deeplearning4j:deeplearning4j-ui:1.0.0-beta7")
    implementation("org.nd4j:nd4j-native-platform:1.0.0-beta7")
//    implementation("org.nd4j:nd4j-native:1.0.0-beta7:windows-x86_64-avx2")
//    implementation("org.deeplearning4j:deeplearning4j-cuda-10.2:1.0.0-beta7")
//    implementation("org.nd4j:nd4j-cuda-10.2-platform:1.0.0-beta7")
    implementation("ch.qos.logback:logback-classic:1.2.3")

    testImplementation(kotlin("test-junit5"))
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "13"
    kotlinOptions.freeCompilerArgs += "-Xopt-in=kotlin.RequiresOptIn"
}

val compileKotlin: KotlinCompile by tasks

compileKotlin.kotlinOptions {
    freeCompilerArgs = listOf("-Xinline-classes")
}

application {
    mainClass.set("com.filipdolnik.mvi.MainKt")
}

tasks.getByName<JavaExec>("run") {
    standardInput = System.`in`
}