package com.filipdolnik.mvi.ai.impl

import com.filipdolnik.mvi.ai.Ai
import com.filipdolnik.mvi.ai.PositionEvaluation
import com.filipdolnik.mvi.domain.Coordinates
import com.filipdolnik.mvi.domain.Position

abstract class BaseAi: Ai {

    private val winningProbabilityEpsilon = 0.01

    override suspend fun suggestNextMove(position: Position): Coordinates =
        suggestNextMoveWithEvaluation(position).first

    override suspend fun suggestNextMoveWithEvaluation(position: Position): Pair<Coordinates, PositionEvaluation> {
        val evaluatedMoves = evaluateAllNextMoves(position)
            .sortedByDescending { it.second }

        val bestMove = evaluatedMoves.firstOrNull() ?: throw IllegalArgumentException("Game does not have any legal moves left.")

        val allBestMoves = evaluatedMoves.filter { bestMove.second.approximatelyCompareTo(it.second, winningProbabilityEpsilon) == 0 }

        return allBestMoves.random().first to bestMove.second
    }
}