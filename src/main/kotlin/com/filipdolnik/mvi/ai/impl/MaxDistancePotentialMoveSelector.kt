package com.filipdolnik.mvi.ai.impl

import com.filipdolnik.mvi.ai.PotentialMoveSelector
import com.filipdolnik.mvi.domain.Coordinates
import com.filipdolnik.mvi.domain.Grid
import com.filipdolnik.mvi.domain.Position

class MaxDistancePotentialMoveSelector(private val maxDistance: Int): PotentialMoveSelector {

    private val adjacentCoordinates =
        Grid.allCoordinates.map { coordinates ->
            (-maxDistance..maxDistance).flatMap { dx ->
                (-maxDistance..maxDistance).mapNotNull { dy ->
                    Coordinates(coordinates.x + dx, coordinates.y + dy)
                }
            }
        }

    override fun selectPotentialMoves(position: Position): List<Coordinates> {
        val moves = HashSet<Coordinates>()

        if (position.moveCount == 0) {
            moves.add(Coordinates.fromValid(Grid.SIZE.width / 2, Grid.SIZE.height / 2))
        } else {
            Grid.allCoordinates.forEach { coordinates ->
                if (position.containsMove(coordinates)) {
                    moves.addAll(adjacentCoordinates[coordinates.linearIndex])
                }
            }
        }

        return moves.filterNot { position.containsMove(it) }
    }
}