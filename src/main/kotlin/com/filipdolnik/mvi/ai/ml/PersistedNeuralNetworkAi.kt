package com.filipdolnik.mvi.ai.ml

import com.filipdolnik.mvi.domain.Player
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork
import java.io.File

class PersistedNeuralNetworkAi(name: String, xEpochIndex: Int?, oEpochIndex: Int?): NeuralNetworkAi() {

    public override val playerXAi: MultiLayerNetwork by lazy {
        MultiLayerNetwork.load(networkFile(name, Player.X, xEpochIndex), false)
    }

    public override val playerOAi: MultiLayerNetwork by lazy {
        MultiLayerNetwork.load(networkFile(name, Player.O, oEpochIndex), false)
    }

    companion object {

        private val classicalAi = PersistedNeuralNetworkAi("classic", null, null)

        private val reinforcementAi = PersistedNeuralNetworkAi("reinforcement", null, null)

        fun classicalAi(): PersistedNeuralNetworkAi = classicalAi

        fun reinforcementAi(): PersistedNeuralNetworkAi = reinforcementAi

        fun networkFile(name: String, type: Player, epochIndex: Int?): File {
            val epochSuffix = if (epochIndex != null) "-$epochIndex" else ""

            return File("src/main/resources/networks/$name-$type$epochSuffix.zip")
        }
    }
}