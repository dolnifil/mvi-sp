package com.filipdolnik.mvi.ai.ml

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork

interface NetworkBuilder {

    fun build(): MultiLayerNetwork
}