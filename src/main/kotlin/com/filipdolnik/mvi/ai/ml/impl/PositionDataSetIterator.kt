package com.filipdolnik.mvi.ai.ml.impl

import com.filipdolnik.mvi.data.PositionGroupWithEvaluation
import com.filipdolnik.mvi.data.PositionWithEvaluation
import com.filipdolnik.mvi.domain.Field
import com.filipdolnik.mvi.domain.Grid
import com.filipdolnik.mvi.domain.Position
import org.nd4j.linalg.dataset.DataSet
import org.nd4j.linalg.dataset.api.DataSetPreProcessor
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator
import org.nd4j.linalg.factory.Nd4j
import java.util.Random
import java.util.concurrent.atomic.AtomicInteger

class PositionDataSetIterator private constructor(
    private val batchSize: Int,
    positionsWithEvaluation: List<PositionWithEvaluation>
): DataSetIterator {

    private var _preProcessor: DataSetPreProcessor? = null

    private val nextBatchStartIndex = AtomicInteger(0)

    private val data = positionsWithEvaluation

    companion object {

        @JvmName("forTrainingGroups")
        fun forTraining(batchSize: Int, positionGroupsWithEvaluation: List<PositionGroupWithEvaluation>): PositionDataSetIterator =
            forTraining(
                batchSize,
                positionGroupsWithEvaluation
                    .flatMap { (positionGroup, evaluation) ->
                        positionGroup.allSymmetricPositions.map { PositionWithEvaluation(it, evaluation) }
                    }
            )

        fun forTraining(batchSize: Int, positionsWithEvaluation: List<PositionWithEvaluation>): PositionDataSetIterator =
            PositionDataSetIterator(
                batchSize,
                positionsWithEvaluation
                    .shuffled(Random(0))
            )

        fun forPrediction(positions: List<Position>): PositionDataSetIterator = forPrediction(positions.size, positions)

        fun forPrediction(batchSize: Int, positions: List<Position>): PositionDataSetIterator = PositionDataSetIterator(
            batchSize,
            positions.map { PositionWithEvaluation(it, 0.5) }
        )
    }

    override fun remove() {
    }

    override fun hasNext(): Boolean = nextBatchStartIndex.get() + batchSize <= data.size

    override fun next(num: Int): DataSet {
        throw IllegalStateException("Not supported")
    }

    override fun next(): DataSet {
        val currentBatchStartIndex = nextBatchStartIndex.getAndAdd(batchSize)
        if (currentBatchStartIndex + batchSize > data.size) {
            throw NoSuchElementException()
        }

        val dataSet = dataSetFrom(currentBatchStartIndex)

        _preProcessor?.preProcess(dataSet)

        return dataSet
    }

    private fun dataSetFrom(currentBatchStartIndex: Int): DataSet {
        val features = Nd4j.zeros(batchSize, 2, Grid.SIZE.height, Grid.SIZE.width)
        val labels = Nd4j.zeros(batchSize, 1)

        data.subList(currentBatchStartIndex, currentBatchStartIndex + batchSize).forEachIndexed { index, (position, evaluation) ->
            Grid.allCoordinates.forEach {
                if (position.containsMove(it)) {
                    val channel = if (position[it] == Field.X) 0 else 1
                    features.putScalar(intArrayOf(index, channel, it.y, it.x), 1.0)
                }
            }

            labels.putScalar(intArrayOf(index, 0), evaluation)
        }

        return DataSet(features, labels)
    }

    override fun inputColumns(): Int = Grid.SIZE.linearSize * 2

    override fun totalOutcomes(): Int = 1

    override fun resetSupported(): Boolean = true

    override fun asyncSupported(): Boolean = true

    override fun reset() {
        nextBatchStartIndex.set(0)
    }

    override fun batch(): Int = batchSize

    override fun setPreProcessor(preProcessor: DataSetPreProcessor?) {
        this._preProcessor = preProcessor
    }

    override fun getPreProcessor(): DataSetPreProcessor? = _preProcessor

    override fun getLabels(): MutableList<String>? = null
}