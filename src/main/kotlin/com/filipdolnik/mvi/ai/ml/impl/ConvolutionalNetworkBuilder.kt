package com.filipdolnik.mvi.ai.ml.impl

import com.filipdolnik.mvi.ai.ml.NetworkBuilder
import com.filipdolnik.mvi.domain.Grid
import org.deeplearning4j.nn.conf.NeuralNetConfiguration
import org.deeplearning4j.nn.conf.inputs.InputType
import org.deeplearning4j.nn.conf.layers.ConvolutionLayer
import org.deeplearning4j.nn.conf.layers.DenseLayer
import org.deeplearning4j.nn.conf.layers.OutputLayer
import org.deeplearning4j.nn.conf.layers.SubsamplingLayer
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork
import org.deeplearning4j.nn.weights.WeightInit
import org.nd4j.linalg.activations.Activation
import org.nd4j.linalg.learning.config.Adam
import org.nd4j.linalg.lossfunctions.LossFunctions

class ConvolutionalNetworkBuilder: NetworkBuilder {

    override fun build(): MultiLayerNetwork {
        val configuration = NeuralNetConfiguration.Builder()
            .seed(0)
            .activation(Activation.LEAKYRELU)
            .weightInit(WeightInit.XAVIER)
            .updater(Adam(0.0005))
            .l2(0.00001)
            .list()
            .layer(
                ConvolutionLayer.Builder(2, 2)
                    .nOut(64)
                    .build()
            )
            .layer(
                ConvolutionLayer.Builder(2, 2)
                    .nOut(128)
                    .build()
            )
            .layer(
                ConvolutionLayer.Builder(2, 2)
                    .nOut(128)
                    .build()
            )
            .layer(
                ConvolutionLayer.Builder(intArrayOf(3, 3), intArrayOf(1, 1), intArrayOf(1, 1))
                    .nOut(256)
                    .build()
            )
            .layer(
                SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
                    .kernelSize(2, 2)
                    .stride(2, 2)
                    .build()
            )
            .layer(
                ConvolutionLayer.Builder(3, 3)
                    .nOut(256)
                    .build()
            )
            .layer(
                SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
                    .kernelSize(2, 2)
                    .stride(2, 2)
                    .build()
            )
            .layer(
                DenseLayer.Builder()
                    .nOut(128)
                    .build()
            )
            .layer(
                DenseLayer.Builder()
                    .nOut(128)
                    .build()
            )
            .layer(
                DenseLayer.Builder()
                    .nOut(32)
                    .build()
            )
            .layer(
                OutputLayer.Builder(LossFunctions.LossFunction.MSE)
                    .activation(Activation.SIGMOID)
                    .nOut(1)
                    .build()
            )
            .setInputType(InputType.convolutional(Grid.SIZE.height.toLong(), Grid.SIZE.width.toLong(), 2))
            .build()

        val model = MultiLayerNetwork(configuration)

        model.init()

        return model
    }
}