package com.filipdolnik.mvi.ai.ml

import ch.qos.logback.classic.Level
import com.filipdolnik.mvi.ai.PositionEvaluation
import com.filipdolnik.mvi.ai.impl.BaseAi
import com.filipdolnik.mvi.ai.impl.MaxDistancePotentialMoveSelector
import com.filipdolnik.mvi.ai.ml.impl.PositionDataSetIterator
import com.filipdolnik.mvi.domain.Coordinates
import com.filipdolnik.mvi.domain.Player
import com.filipdolnik.mvi.domain.Position
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork
import org.nd4j.common.config.ND4JEnvironmentVars
import org.nd4j.common.config.ND4JSystemProperties
import org.slf4j.Logger
import org.slf4j.LoggerFactory

abstract class NeuralNetworkAi: BaseAi() {

    protected abstract val playerXAi: MultiLayerNetwork
    protected abstract val playerOAi: MultiLayerNetwork

    private val potentialMoveSelector = MaxDistancePotentialMoveSelector(2)

    override suspend fun evaluateAllNextMoves(position: Position): List<Pair<Coordinates, PositionEvaluation>> {
        val otherPlayerAi = if (position.currentPlayer == Player.X) playerOAi else playerXAi

        val potentialMoves = potentialMoveSelector.selectPotentialMoves(position)
        val potentialPositions = potentialMoves.map { positionAfterMove(position, it) }
        val encodedPotentialPositions = PositionDataSetIterator.forPrediction(potentialPositions)

        val predictions = withContext(Dispatchers.IO) {
            otherPlayerAi.output(encodedPotentialPositions)
        }

        return potentialMoves.mapIndexed { index, move ->
            val prediction = predictions.getDouble(index, 0).coerceIn(0.0, 1.0)

            move to PositionEvaluation(prediction, 1).reversePlayerPerspective()
        }
    }

    override suspend fun evaluateMove(position: Position, move: Coordinates): PositionEvaluation {
        val positionAfterMove = positionAfterMove(position, move)

        return evaluatePosition(positionAfterMove).reversePlayerPerspective()
    }

    private fun positionAfterMove(position: Position, move: Coordinates): Position {
        if (position.containsMove(move)) {
            throw IllegalArgumentException("Cannot evaluate already played move $move.")
        }

        val positionCopy = position.clone()

        positionCopy[move] = positionCopy.currentPlayer.field
        return positionCopy
    }

    override suspend fun evaluatePosition(position: Position): PositionEvaluation {
        val ai = if (position.currentPlayer == Player.X) playerXAi else playerOAi

        val encodedPosition = PositionDataSetIterator.forPrediction(listOf(position))

        val prediction = ai.output(encodedPosition).getDouble(0, 0).coerceIn(0.0, 1.0)

        return PositionEvaluation(prediction, 1)
    }

    companion object {

        fun disableDebug(level: Level = Level.WARN) {
            System.setProperty(ND4JSystemProperties.LOG_INITIALIZATION, "false")
            System.setProperty(ND4JSystemProperties.ND4J_IGNORE_AVX, "true")
            System.setProperty(ND4JSystemProperties.VERSION_CHECK_PROPERTY, "false")
            (LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME) as ch.qos.logback.classic.Logger).level = level
        }

        fun enableDebug(level: Level = Level.DEBUG) {
            System.setProperty(ND4JSystemProperties.LOG_INITIALIZATION, "true")
            System.setProperty(ND4JSystemProperties.ND4J_IGNORE_AVX, "false")
            System.setProperty(ND4JSystemProperties.VERSION_CHECK_PROPERTY, "true")
            (LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME) as ch.qos.logback.classic.Logger).level = level
        }
    }
}