package com.filipdolnik.mvi.ai.ml

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork

class LoadedNeuralNetworkAi(
    override val playerXAi: MultiLayerNetwork,
    override val playerOAi: MultiLayerNetwork
): NeuralNetworkAi()