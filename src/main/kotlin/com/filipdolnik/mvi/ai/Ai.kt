package com.filipdolnik.mvi.ai

import com.filipdolnik.mvi.domain.Coordinates
import com.filipdolnik.mvi.domain.Position

interface Ai: PositionEvaluator {

    suspend fun suggestNextMove(position: Position): Coordinates

    suspend fun suggestNextMoveWithEvaluation(position: Position): Pair<Coordinates, PositionEvaluation>

    suspend fun evaluateAllNextMoves(position: Position): List<Pair<Coordinates, PositionEvaluation>>

    suspend fun evaluateMove(position: Position, move: Coordinates): PositionEvaluation
}