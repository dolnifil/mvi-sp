package com.filipdolnik.mvi.ai

import com.filipdolnik.mvi.domain.Position

interface PositionEvaluator {

    suspend fun evaluatePosition(position: Position): PositionEvaluation
}