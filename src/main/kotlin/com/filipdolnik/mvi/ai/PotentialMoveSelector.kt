package com.filipdolnik.mvi.ai

import com.filipdolnik.mvi.domain.Coordinates
import com.filipdolnik.mvi.domain.Position

interface PotentialMoveSelector {

    fun selectPotentialMoves(position: Position): List<Coordinates>
}