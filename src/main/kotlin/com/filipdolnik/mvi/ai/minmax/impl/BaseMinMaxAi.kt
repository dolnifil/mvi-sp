package com.filipdolnik.mvi.ai.minmax.impl

import com.filipdolnik.mvi.ai.PositionEvaluation
import com.filipdolnik.mvi.ai.PositionEvaluator
import com.filipdolnik.mvi.ai.PotentialMoveSelector
import com.filipdolnik.mvi.ai.impl.BaseAi
import com.filipdolnik.mvi.ai.impl.MaxDistancePotentialMoveSelector
import com.filipdolnik.mvi.domain.Coordinates
import com.filipdolnik.mvi.domain.Position
import com.filipdolnik.mvi.util.parallelMap

abstract class BaseMinMaxAi(
    private val positionEvaluator: PositionEvaluator,
    private val maxDepth: Int = 3,
    private val maxWidth: Int = 30,
    private val potentialMoveSelector: PotentialMoveSelector = MaxDistancePotentialMoveSelector(2)
): BaseAi() {

    override suspend fun evaluateAllNextMoves(position: Position): List<Pair<Coordinates, PositionEvaluation>> =
        evaluateAllNextMoves(position, maxDepth)

    override suspend fun evaluateMove(position: Position, move: Coordinates): PositionEvaluation {
        val positionCopy = position.clone()

        positionCopy[move] = positionCopy.currentPlayer.field

        return evaluatePosition(positionCopy, maxDepth - 1).reversePlayerPerspective()
    }

    override suspend fun evaluatePosition(position: Position): PositionEvaluation = evaluatePosition(position, maxDepth)

    private suspend fun evaluatePosition(position: Position, maxDepth: Int): PositionEvaluation =
        evaluateAllNextMoves(position, maxDepth).maxOfOrNull { it.second } ?: PositionEvaluation.draw(0)

    private suspend fun evaluateAllNextMoves(position: Position, maxDepth: Int): List<Pair<Coordinates, PositionEvaluation>> {
        val root = StateNode(position.clone(), PositionEvaluation.draw(0))

        for (iteration in 1..maxDepth) {
            root.explore(iteration, 2, ignoreSolved = true)
        }

        return root.transitions.map { (coordinates, state) ->
            coordinates to state.evaluation.reversePlayerPerspective()
        }
    }

    private data class Transition(val coordinates: Coordinates, val state: StateNode)

    private inner class StateNode(val position: Position, evaluation: PositionEvaluation) {

        val isSolved: Boolean
            get() = evaluation.isWinning || evaluation.isLosing

        var transitions: List<Transition> = emptyList()
            private set

        var evaluation: PositionEvaluation = evaluation
            private set

        suspend fun explore(maxDepth: Int, parallelDepth: Int, ignoreSolved: Boolean = false) {
            if (maxDepth <= 0 || (isSolved && !ignoreSolved)) {
                return
            }

            if (transitions.isEmpty()) {
                val potentialMoves = potentialMoveSelector.selectPotentialMoves(position)
                transitions = if (parallelDepth > 0) {
                    potentialMoves.parallelMap { createNewTransition(it, positionEvaluator) }
                } else {
                    potentialMoves.map { createNewTransition(it, positionEvaluator) }
                }
            }

            if (parallelDepth > 0) {
                transitions.parallelMap { it.state.explore(maxDepth - 1, parallelDepth - 1) }
            } else {
                transitions.forEach { it.state.explore(maxDepth - 1, parallelDepth - 1) }
            }

            thinTransitions()

            evaluation = transitions.minOfOrNull { it.state.evaluation }
                ?.reversePlayerPerspective()
                ?.let { it.copy(moveDepth = it.moveDepth + 1) } ?: evaluation
        }

        private suspend fun createNewTransition(coordinates: Coordinates, positionEvaluator: PositionEvaluator): Transition {
            val positionCopy = position.clone()

            positionCopy[coordinates] = positionCopy.currentPlayer.field

            val evaluation = positionEvaluator.evaluatePosition(positionCopy)
            val state = StateNode(positionCopy, evaluation)

            return Transition(coordinates, state)
        }

        private fun thinTransitions() {
            if (transitions.size > maxWidth) {
                transitions = transitions.sortedBy { it.state.evaluation }.take(maxWidth)
            }
        }
    }
}