package com.filipdolnik.mvi.ai.minmax

import com.filipdolnik.mvi.ai.minmax.impl.BaseMinMaxAi
import com.filipdolnik.mvi.ai.position.CurrentPositionAi

class MinMaxCurrentPositionAi: BaseMinMaxAi(CurrentPositionAi())
