package com.filipdolnik.mvi.ai

data class PositionEvaluation(val winningProbability: Double, val moveDepth: Int): Comparable<PositionEvaluation> {

    val isWinning: Boolean
        get() = winningProbability >= 1.0

    val isLosing: Boolean
        get() = winningProbability <= 0.0

    val mergedEvaluation: Double
        get() = when {
            isWinning -> (1 - ((moveDepth - 1) * 2 / 100.0)).coerceIn(0.9, 1.0)
            isLosing -> ((moveDepth - 1) * 2 / 100.0).coerceIn(0.0, 0.1)
            else -> winningProbability
        }

    init {
        require(winningProbability in 0.0..1.0)
    }

    fun reversePlayerPerspective(): PositionEvaluation = PositionEvaluation(1 - winningProbability, moveDepth)

    override fun compareTo(other: PositionEvaluation): Int {
        return if (winningProbability == other.winningProbability) {
            if (winningProbability >= 0.5) {
                other.moveDepth - moveDepth
            } else {
                moveDepth - other.moveDepth
            }
        } else if (winningProbability < other.winningProbability) {
            -1
        } else {
            1
        }
    }

    fun approximatelyCompareTo(other: PositionEvaluation, epsilon: Double): Int {
        val roundedWinningProbability = (winningProbability / epsilon).toInt()
        val roundedOtherWinningProbability = (other.winningProbability / epsilon).toInt()

        return if (roundedWinningProbability == roundedOtherWinningProbability) {
            if (winningProbability >= 0.5) {
                other.moveDepth - moveDepth
            } else {
                moveDepth - other.moveDepth
            }
        } else {
            roundedWinningProbability - roundedOtherWinningProbability
        }
    }

    companion object {

        fun winning(moveDepth: Int): PositionEvaluation = PositionEvaluation(1.0, moveDepth)

        fun losing(moveDepth: Int): PositionEvaluation = PositionEvaluation(0.0, moveDepth)

        fun draw(moveDepth: Int): PositionEvaluation = PositionEvaluation(0.5, moveDepth)
    }
}