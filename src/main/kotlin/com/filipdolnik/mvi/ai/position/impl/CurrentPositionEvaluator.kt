package com.filipdolnik.mvi.ai.position.impl

import com.filipdolnik.mvi.ai.PositionEvaluation
import com.filipdolnik.mvi.ai.PositionEvaluator
import com.filipdolnik.mvi.ai.impl.MaxDistancePotentialMoveSelector
import com.filipdolnik.mvi.domain.Coordinates
import com.filipdolnik.mvi.domain.Field
import com.filipdolnik.mvi.domain.Game
import com.filipdolnik.mvi.domain.Grid
import com.filipdolnik.mvi.domain.Position

class CurrentPositionEvaluator: PositionEvaluator {

    private val maxDistancePotentialMoveSelector = MaxDistancePotentialMoveSelector(1)

    override suspend fun evaluatePosition(position: Position): PositionEvaluation {
        val finalPositionEvaluation = evaluateFinalPosition(position)
        if (finalPositionEvaluation != null) {
            return finalPositionEvaluation
        }

        val fieldsProperties = getFieldsProperties(position)

        reduceFieldsProperties(fieldsProperties)

        return evaluateGlobalFieldsProperties(fieldsProperties) ?: evaluateLocalFieldsProperties(fieldsProperties)
    }

    private fun evaluateFinalPosition(position: Position): PositionEvaluation? {
        if (position.moveCount == Grid.SIZE.linearSize) {
            return PositionEvaluation.draw(0)
        }

        Grid.allCoordinates.forEach {
            if (position.containsMove(it) && Game.isWinningField(position, it)) {
                return if (position.currentPlayer.field == position[it]) {
                    PositionEvaluation.winning(0)
                } else {
                    PositionEvaluation.losing(0)
                }
            }
        }

        return null
    }

    private fun getFieldsProperties(position: Position): List<ArrayList<FieldProperty>> {
        val currentPlayerField = position.currentPlayer.field
        val opponentField = position.currentPlayer.opponent.field

        return maxDistancePotentialMoveSelector.selectPotentialMoves(position).map {
            val fieldProperties = ArrayList<FieldProperty>()

            extractFieldProperties(position, it, currentPlayerField, opponentField, fieldProperties)

            fieldProperties
        }
    }

    private fun extractFieldProperties(
        position: Position,
        coordinates: Coordinates,
        currentPlayerField: Field,
        opponentField: Field,
        fieldProperties: ArrayList<FieldProperty>
    ) {
        Grid.allNeighborhoodPathsWithCenter(coordinates).forEach { path ->
            getFieldPropertyFromPathForPlayer(position, path, opponentField)?.let {
                fieldProperties.add(it)
            }
            getFieldPropertyFromPathForPlayer(position, path, currentPlayerField)?.let {
                val mappedFieldProperty = mapCurrentPlayerFieldPropertyToOpponent(it)

                fieldProperties.add(mappedFieldProperty)
            }
        }
    }

    private fun getFieldPropertyFromPathForPlayer(
        position: Position,
        path: Grid.Path,
        otherPlayerField: Field
    ): FieldProperty? {
        val pathProperties = getPathProperties(position, otherPlayerField, path)
        val currentFieldCount = pathProperties.currentFieldCount
        val potentialFieldCount = pathProperties.potentialFieldCount

        return when {
            currentFieldCount > 4 || currentFieldCount == 0 || potentialFieldCount < 4 -> null
            currentFieldCount == 4 -> FieldProperty.W1
            currentFieldCount == 3 -> if (pathProperties.isOpen) FieldProperty.W2 else FieldProperty.A1
            currentFieldCount == 2 -> if (pathProperties.isOpen) FieldProperty.A1 else FieldProperty.A2
            currentFieldCount == 1 -> if (pathProperties.isOpen) FieldProperty.A2 else FieldProperty.A3
            else -> throw IllegalStateException("Cases above should be exhaustive. $pathProperties")
        }
    }

    private fun mapCurrentPlayerFieldPropertyToOpponent(fieldProperty: FieldProperty): FieldProperty = when (fieldProperty) {
        FieldProperty.W1 -> FieldProperty.L1
        FieldProperty.W2 -> FieldProperty.L2
        FieldProperty.A1 -> FieldProperty.D1
        FieldProperty.A2 -> FieldProperty.D2
        FieldProperty.A3 -> FieldProperty.D3
        else -> throw IllegalArgumentException("Only properties for empty field and current player can mapped.")
    }

    private fun getPathProperties(position: Position, otherPlayerField: Field, path: Grid.Path): PathProperties {
        fun scanRightPath(rightPath: List<Coordinates>): Triple<Int, Int, Boolean> {
            var potentialFieldCount = 0
            var currentFieldCount = 0
            var isOpen = false

            for (coordinates in rightPath) {
                val field = position[coordinates]
                if (field == otherPlayerField) {
                    break
                }

                potentialFieldCount++
                if (field == Field.Empty) {
                    isOpen = true
                }
                if (!isOpen) {
                    currentFieldCount++
                }
            }

            return Triple(potentialFieldCount, currentFieldCount, isOpen)
        }

        val (leftPotentialFieldCount, leftCurrentFieldCount, isLeftOpen) = scanRightPath(path.reversedLeftPart)
        val (rightPotentialFieldCount, rightCurrentFieldCount, isRightOpen) = scanRightPath(path.rightPath)

        val potentialFieldCount = leftPotentialFieldCount + rightPotentialFieldCount
        val currentFieldCount = leftCurrentFieldCount + rightCurrentFieldCount
        val isOpen = isLeftOpen && isRightOpen

        return PathProperties(currentFieldCount, potentialFieldCount, isOpen)
    }

    private fun reduceFieldsProperties(fieldsProperties: List<ArrayList<FieldProperty>>) {
        fieldsProperties.forEach { fieldProperties ->
            var isW1 = false
            var isL1 = false
            var a1Count = 0
            var d1Count = 0

            for (fieldProperty in fieldProperties) {
                when (fieldProperty) {
                    FieldProperty.W1 -> isW1 = true
                    FieldProperty.L1 -> isL1 = true
                    FieldProperty.A1 -> a1Count++
                    FieldProperty.D1 -> d1Count++
                    else -> {
                    }
                }
            }

            if (isW1) {
                fieldProperties.clear()
                fieldProperties.add(FieldProperty.W1)
            } else if (isL1) {
                fieldProperties.removeIf { it != FieldProperty.W2 }
                fieldProperties.add(FieldProperty.L1)
            } else {
                if (a1Count > 1) {
                    fieldProperties.removeIf { it == FieldProperty.A1 }
                    fieldProperties.add(FieldProperty.W3)
                }

                if (d1Count > 1) {
                    fieldProperties.removeIf { it == FieldProperty.D1 }
                    fieldProperties.add(FieldProperty.L3)
                }
            }
        }
    }

    private fun evaluateGlobalFieldsProperties(fieldsProperties: List<List<FieldProperty>>): PositionEvaluation? {
        var isW1 = false
        var isW2 = false
        var isW3 = false
        var isLocallyW2andL1 = false
        var isLocallyW3andL2orL1 = false
        var l1Count = 0
        var l2Count = 0

        for (fieldProperties in fieldsProperties) {
            var isFieldW2 = false
            var isFieldW3 = false
            var isFieldL1 = false
            var isFieldL2 = false

            for (fieldProperty in fieldProperties) {
                when (fieldProperty) {
                    FieldProperty.W1 -> isW1 = true
                    FieldProperty.W2 -> {
                        isW2 = true
                        isFieldW2 = true
                    }
                    FieldProperty.W3 -> {
                        isW3 = true
                        isFieldW3 = true
                    }
                    FieldProperty.L1 -> {
                        l1Count++
                        isFieldL1 = true
                    }
                    FieldProperty.L2 -> {
                        l2Count++
                        isFieldL2 = true
                    }
                    else -> {
                    }
                }
            }

            if (isFieldW2 && isFieldL1) {
                isLocallyW2andL1 = true
            }
            if (isFieldW3 && (isFieldL1 || isFieldL2)) {
                isLocallyW3andL2orL1 = true
            }
        }

        return when {
            isW1 -> PositionEvaluation.winning(1)
            l1Count > 1 -> PositionEvaluation.losing(2)
            (l1Count == 0 && isW2) || isLocallyW2andL1 -> PositionEvaluation.winning(3)
            (l1Count + l2Count == 0 && isW3) || (l1Count + l2Count == 1 && isLocallyW3andL2orL1) -> PositionEvaluation.winning(5)
            else -> null
        }
    }

    private fun evaluateLocalFieldsProperties(properties: List<List<FieldProperty>>): PositionEvaluation {
        val localFieldsEvaluation = properties.sumBy { evaluateLocalFieldProperties(it) }.coerceIn(-400, 400) / 1000.0

        return PositionEvaluation(0.5 + localFieldsEvaluation, 1)
    }

    private fun evaluateLocalFieldProperties(properties: List<FieldProperty>): Int = properties.sumBy { assignValueToFieldProperty(it) }

    private fun assignValueToFieldProperty(property: FieldProperty): Int = when (property) {
        FieldProperty.W1 -> 1000
        FieldProperty.W2 -> 500
        FieldProperty.W3 -> 500
        FieldProperty.L1 -> -500
        FieldProperty.L2 -> -150
        FieldProperty.L3 -> -150
        FieldProperty.A1 -> 75
        FieldProperty.A2 -> 10
        FieldProperty.A3 -> 5
        FieldProperty.D1 -> -50
        FieldProperty.D2 -> -5
        FieldProperty.D3 -> -1
    }

    private data class PathProperties(val currentFieldCount: Int, val potentialFieldCount: Int, val isOpen: Boolean)

    private enum class FieldProperty {
        W1, W2, W3, // Winning field in 1/2/3 moves
        L1, L2, L3, // Losing field in 1/2/3 opponent's moves
        A1, A2, A3, // Forcing move in 1/2/3 moves
        D1, D2, D3 // Opponent forces move in 1/2/3 moves
    }
}