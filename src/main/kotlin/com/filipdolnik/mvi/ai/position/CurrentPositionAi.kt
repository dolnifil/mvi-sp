package com.filipdolnik.mvi.ai.position

import com.filipdolnik.mvi.ai.PositionEvaluation
import com.filipdolnik.mvi.ai.impl.BaseAi
import com.filipdolnik.mvi.ai.impl.MaxDistancePotentialMoveSelector
import com.filipdolnik.mvi.ai.position.impl.CurrentPositionEvaluator
import com.filipdolnik.mvi.domain.Coordinates
import com.filipdolnik.mvi.domain.Position
import com.filipdolnik.mvi.util.parallelMap

class CurrentPositionAi: BaseAi() {

    private val positionEvaluator = CurrentPositionEvaluator()
    private val potentialMoveSelector = MaxDistancePotentialMoveSelector(2)

    override suspend fun evaluateAllNextMoves(position: Position): List<Pair<Coordinates, PositionEvaluation>> =
        potentialMoveSelector.selectPotentialMoves(position)
            .parallelMap { it to evaluateMove(position, it) }

    override suspend fun evaluateMove(position: Position, move: Coordinates): PositionEvaluation {
        if (position.containsMove(move)) {
            throw IllegalArgumentException("Cannot evaluate already played move $move.")
        }

        val positionCopy = position.clone()

        positionCopy[move] = positionCopy.currentPlayer.field

        return positionEvaluator.evaluatePosition(positionCopy).reversePlayerPerspective()
    }

    override suspend fun evaluatePosition(position: Position): PositionEvaluation =
        positionEvaluator.evaluatePosition(position)
}