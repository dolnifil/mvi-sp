package com.filipdolnik.mvi.view

import com.filipdolnik.mvi.domain.Coordinates
import com.filipdolnik.mvi.domain.GameResult
import com.filipdolnik.mvi.domain.Player

open class GameView {

    private val gameStateView = GameStateView()

    fun printHelp() {
        println("Enter Z to revert last move, E to end the game. Move has format: \"x,y\"")
    }

    fun requestNextAction(currentPlayer: Player): Action {
        printNextMoveDialog(currentPlayer)

        while (true) {
            val action = readLine()?.let { parseAction(it) }

            if (action != null) {
                println()

                return action
            } else {
                println("Incorrect action. For move use: \"x,y\"")
            }
        }
    }

    private fun printNextMoveDialog(currentPlayer: Player) {
        print("Enter move for player $currentPlayer: ")
    }

    private fun parseAction(action: String): Action? {
        val coordinates = parseCoordinates(action)
        return if (coordinates != null) {
            Action.Move(coordinates)
        } else {
            when {
                action.toUpperCase().startsWith("Z") -> Action.Revert
                action.toUpperCase().startsWith("E") -> Action.End
                else -> null
            }
        }
    }

    private fun parseCoordinates(move: String): Coordinates? {
        val input = move.split(",").map { it.toIntOrNull() }

        val x = input.getOrNull(0) ?: return null
        val y = input.getOrNull(1) ?: return null

        return Coordinates(x, y)
    }

    fun requestEndedGameAction(): EndedGameAction {
        println("Press enter to continue. Use Z to revert last move.")

        return readLine()?.toUpperCase()?.let {
            if (it.startsWith("Z")) EndedGameAction.Revert else null
        } ?: EndedGameAction.End
    }

    fun printGameState(gameStateDescription: GameStateView.GameStateDescription) {
        gameStateView.printGameState(gameStateDescription)
        println()
    }

    fun printGameResult(gameResult: GameResult, moveCount: Int) {
        println("Game over. Result: $gameResult. Move count: $moveCount")
    }

    fun printGameEndedWithoutResult() {
        println("Game ended without result.")
    }

    fun printError(throwable: Throwable) {
        println("Error: ${throwable.message ?: throwable.toString()}")
    }

    sealed class Action {

        data class Move(val coordinates: Coordinates): Action()

        object Revert: Action()

        object End: Action()
    }

    sealed class EndedGameAction {

        object Revert: EndedGameAction()

        object End: EndedGameAction()
    }
}