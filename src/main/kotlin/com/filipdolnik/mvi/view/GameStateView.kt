package com.filipdolnik.mvi.view

import com.filipdolnik.mvi.domain.Coordinates
import com.filipdolnik.mvi.domain.Grid

class GameStateView {

    interface GameStateDescription {

        val maxColumnWidth: Int

        fun contentAt(coordinates: Coordinates): String
    }

    fun printGameState(gameStateDescription: GameStateDescription) {
        printColumnIndexRow(gameStateDescription)

        printSeparatorLine(gameStateDescription)

        repeat(Grid.SIZE.height) {
            printRow(gameStateDescription, it)
        }
    }

    private fun printSeparatorLine(gameStateDescription: GameStateDescription) {
        repeat((getMaxAdjustedColumnWidth(gameStateDescription) + 1) * Grid.SIZE.width + 5) {
            print("=")
        }

        println()
    }

    private fun printColumnIndexRow(gameStateDescription: GameStateDescription) {
        printCornerCell()

        repeat(Grid.SIZE.width) {
            printColumnIndexCell(gameStateDescription, it)
        }

        println()
    }

    private fun printRow(gameStateDescription: GameStateDescription, index: Int) {
        printRowIndexCell(index)

        repeat(Grid.SIZE.width) {
            printFieldCell(gameStateDescription, Coordinates.fromValid(it, index))
        }

        println()
    }

    private fun printCornerCell() {
        print("   ||")
    }

    private fun printRowIndexCell(index: Int) {
        val text = index.toString()
        val paddedText = if (text.length == 1) " $text" else text

        print("$paddedText ||")
    }

    private fun printColumnIndexCell(gameStateDescription: GameStateDescription, index: Int) {
        printCell(gameStateDescription, index.toString())
    }

    private fun printFieldCell(gameStateDescription: GameStateDescription, coordinates: Coordinates) {
        printCell(gameStateDescription, gameStateDescription.contentAt(coordinates))
    }

    private fun printCell(gameStateDescription: GameStateDescription, content: String) {
        val centeredContent = if (content.length % 2 == 0) " $content" else content
        val targetSize = getMaxAdjustedColumnWidth(gameStateDescription)
        val remainingPadding = (targetSize - centeredContent.length) / 2

        print(centeredContent.padStart(targetSize - remainingPadding).padEnd(targetSize) + "|")
    }

    private fun getMaxAdjustedColumnWidth(gameStateDescription: GameStateDescription): Int =
        (gameStateDescription.maxColumnWidth / 2) * 2 + 1
}