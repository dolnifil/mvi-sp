package com.filipdolnik.mvi.view

import com.filipdolnik.mvi.ai.AiFactory
import com.filipdolnik.mvi.ai.minmax.MinMaxCurrentPositionAi
import com.filipdolnik.mvi.ai.ml.NeuralNetworkAi
import com.filipdolnik.mvi.ai.ml.PersistedNeuralNetworkAi
import com.filipdolnik.mvi.ai.position.CurrentPositionAi

object AiSelectionView {

    fun selectAi(): AiFactory {
        while (true) {
            println("Select Ai:")
            println("1: CurrentPositionAi")
            println("2: MinMaxAi")
            println("3: Classic NN")
            println("4: Reinforcement NN")

            val aiFactory: AiFactory = when (readLine()?.toIntOrNull()) {
                1 -> ::CurrentPositionAi
                2 -> ::MinMaxCurrentPositionAi
                3 -> {
                    { PersistedNeuralNetworkAi.classicalAi() }
                }
                4 -> {
                    { PersistedNeuralNetworkAi.reinforcementAi() }
                }
                else -> {
                    println("Wrong option.")
                    continue
                }
            }

            println()

            return aiFactory
        }
    }
}