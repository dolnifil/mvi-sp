package com.filipdolnik.mvi.view

class PlayerVsAiGameView: GameView() {

    fun decideIfPlayerStarts(): Boolean {
        while (true) {
            print("Player starts (y/n): ")

            when (readLine()?.toUpperCase()?.firstOrNull()) {
                'Y' -> return true
                'N' -> return false
                else -> println("Invalid option.")
            }
        }
    }
}