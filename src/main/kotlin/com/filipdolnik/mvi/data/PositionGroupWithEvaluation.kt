package com.filipdolnik.mvi.data

data class PositionGroupWithEvaluation(val positionGroup: PositionGroup, val evaluation: Double)