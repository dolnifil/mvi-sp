package com.filipdolnik.mvi.data

import com.filipdolnik.mvi.domain.Coordinates
import com.filipdolnik.mvi.domain.Field
import com.filipdolnik.mvi.domain.Grid
import com.filipdolnik.mvi.domain.Position

class PositionGroup(basePosition: Position) {

    val allSymmetricPositions: List<Position>

    val moveCount: Int
        get() = canonicalPosition.moveCount

    val canonicalPosition: Position
        get() = allSymmetricPositions.first()

    init {
        fun mirror(position: Position): Position {
            val mirrorPosition = Position()

            Grid.allCoordinates.forEach {
                if (position.containsMove(it)) {
                    mirrorPosition[Coordinates.fromValid(it.y, it.x)] = position[it]
                }
            }

            return mirrorPosition
        }

        fun rotate(position: Position): Position {
            val rotatedPosition = Position()

            Grid.allCoordinates.forEach {
                if (position.containsMove(it)) {
                    rotatedPosition[Coordinates.fromValid(Grid.SIZE.width - 1 - it.y, it.x)] = position[it]
                }
            }

            return rotatedPosition
        }

        val r1 = rotate(basePosition)
        val r2 = rotate(r1)
        val r3 = rotate(r2)
        val m0 = mirror(basePosition)
        val m1 = rotate(m0)
        val m2 = rotate(m1)
        val m3 = rotate(m2)

        allSymmetricPositions = listOf(basePosition, r1, r2, r3, m0, m1, m2, m3).sortedWith(CanonicalPositionComparator)
    }

    fun isPastOf(positionGroup: PositionGroup): Boolean =
        allSymmetricPositions.any { it.isPastOf(positionGroup.canonicalPosition) }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PositionGroup

        if (canonicalPosition != other.canonicalPosition) return false

        return true
    }

    override fun hashCode(): Int {
        return canonicalPosition.hashCode()
    }

    private object CanonicalPositionComparator: Comparator<Position> {

        override fun compare(o1: Position, o2: Position): Int {
            Grid.allCoordinates.forEach {
                val o1Field = o1[it]
                val o2Field = o2[it]

                if (o1Field != o2Field) {
                    return when (o1Field) {
                        Field.Empty -> 1
                        Field.X -> -1
                        Field.O -> if (o2Field == Field.X) 1 else -1
                    }
                }
            }

            return 0
        }
    }

    companion object {

    }
}
