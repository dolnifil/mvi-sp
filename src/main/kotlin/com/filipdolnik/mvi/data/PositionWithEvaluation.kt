package com.filipdolnik.mvi.data

import com.filipdolnik.mvi.domain.Position

data class PositionWithEvaluation(val position: Position, val evaluation: Double)