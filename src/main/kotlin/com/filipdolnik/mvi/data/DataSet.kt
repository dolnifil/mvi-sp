package com.filipdolnik.mvi.data

import com.filipdolnik.mvi.data.impl.Storage
import com.filipdolnik.mvi.domain.Player

class DataSet private constructor(val name: String, type: Player) {

    val data: List<PositionGroupWithEvaluation>
        get() = _data

    private val _data = ArrayList<PositionGroupWithEvaluation>()

    private val knownPositionGroups = HashMap<PositionGroup, Int>()

    private val storage = Storage("$name-$type")

    fun add(positionGroupWithEvaluation: PositionGroupWithEvaluation) {
        val index = knownPositionGroups[positionGroupWithEvaluation.positionGroup]

        if (index != null) {
            _data[index] = positionGroupWithEvaluation
        } else {
            knownPositionGroups[positionGroupWithEvaluation.positionGroup] = _data.size
            _data.add(positionGroupWithEvaluation)
        }
    }

    fun addAll(positionGroupWithEvaluations: List<PositionGroupWithEvaluation>) {
        positionGroupWithEvaluations.forEach {
            add(it)
        }
    }

    suspend fun save() {
        storage.save(data)
    }

    private suspend fun load() {
        val loadedData = storage.load()

        addAll(loadedData)
    }

    companion object {

        suspend operator fun invoke(name: String, type: Player): DataSet {
            val dataset = DataSet(name, type)

            dataset.load()

            return dataset
        }

        suspend fun endGames(type: Player): DataSet = invoke("end-games", type)

        suspend fun finalLoosingMoves(type: Player): DataSet = invoke("final-loosing-moves", type)

        suspend fun finalWinningMoves(type: Player): DataSet = invoke("final-winning-moves", type)

        suspend fun finalPositions(type: Player): DataSet = invoke("final-positions", type)

        suspend fun preFinalPositions(type: Player): DataSet = invoke("pre-final-positions", type)

        suspend fun problematicPositions(type: Player): DataSet = invoke("problematic-positions", type)

        suspend fun games(type: Player): DataSet = invoke("games", type)

        suspend fun gamesWithOffset(type: Player): DataSet = invoke("games-with-offset", type)

        suspend fun gamesWithMoves(type: Player): DataSet = invoke("games-with-moves", type)

        suspend fun testing(type: Player): DataSet = invoke("testing-dataset", type)
    }
}