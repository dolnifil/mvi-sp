package com.filipdolnik.mvi.data.generator

import com.filipdolnik.mvi.data.generator.internal.BaseDataGenerator
import com.filipdolnik.mvi.domain.Position

class GamesDataGenerator: BaseDataGenerator() {

    override suspend fun selectPositions(positionsInGameOrder: List<Position>): List<Position> = positionsInGameOrder
}