package com.filipdolnik.mvi.data.generator

import com.filipdolnik.mvi.ai.impl.MaxDistancePotentialMoveSelector
import com.filipdolnik.mvi.data.generator.internal.BaseDataGenerator
import com.filipdolnik.mvi.data.generator.internal.PotentialMoveDataGenerator
import com.filipdolnik.mvi.domain.Position

class EndGameWithMovesDataGenerator: PotentialMoveDataGenerator(MaxDistancePotentialMoveSelector(2)) {

    override suspend fun selectPositions(positionsInGameOrder: List<Position>): List<Position> =
        positionsInGameOrder.dropLast(1).takeLast(10)
}