package com.filipdolnik.mvi.data.generator.internal

import com.filipdolnik.mvi.ai.Ai
import com.filipdolnik.mvi.ai.minmax.MinMaxCurrentPositionAi
import com.filipdolnik.mvi.ai.position.CurrentPositionAi
import com.filipdolnik.mvi.data.PositionGroup
import com.filipdolnik.mvi.data.generator.DataGenerator
import com.filipdolnik.mvi.domain.Game
import com.filipdolnik.mvi.domain.Player
import com.filipdolnik.mvi.domain.Position
import com.filipdolnik.mvi.util.parallelMap

abstract class BaseDataGenerator: DataGenerator {

    override val recommendedBatchSize: Int = 5000

    override val targetDataSetSize: Int = 100_000

    override val supportedTypes: List<Player> = Player.values().toList()

    override suspend fun generate(gameCount: Int): List<PositionGroup> =
        (0 until gameCount)
            .parallelMap {
                generatePositionsFromPlayingGame().map { PositionGroup(it) }
            }
            .flatten()

    private suspend fun generatePositionsFromPlayingGame(): List<Position> {
        val playerXAi = createPlayerXAi()
        val playerOAi = createPlayerOAi()

        val positions = ArrayList<Position>()
        val game = Game(getInitialPosition())

        while (game.isNotEnded && continuePlaying(game)) {
            val currentAi = if (game.currentPlayer == Player.X) playerXAi else playerOAi

            val move = currentAi.suggestNextMove(game.position)

            game.playNextMove(move)

            positions.add(game.position.clone())
        }

        return selectPositions(positions)
    }

    protected abstract suspend fun selectPositions(positionsInGameOrder: List<Position>): List<Position>

    protected open fun continuePlaying(game: Game): Boolean = true

    protected open fun getInitialPosition(): Position = Position()

    protected open fun createPlayerXAi(): Ai = if (Math.random() < 0.01) MinMaxCurrentPositionAi() else CurrentPositionAi()

    protected open fun createPlayerOAi(): Ai = if (Math.random() < 0.01) MinMaxCurrentPositionAi() else CurrentPositionAi()
}