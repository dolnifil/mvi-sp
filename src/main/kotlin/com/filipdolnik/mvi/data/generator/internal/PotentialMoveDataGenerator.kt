package com.filipdolnik.mvi.data.generator.internal

import com.filipdolnik.mvi.ai.PotentialMoveSelector
import com.filipdolnik.mvi.data.PositionGroup
import com.filipdolnik.mvi.data.generator.DataGenerator
import com.filipdolnik.mvi.domain.Game
import com.filipdolnik.mvi.domain.Position

abstract class PotentialMoveDataGenerator(private val potentialMoveSelector: PotentialMoveSelector): BaseDataGenerator() {

    override val recommendedBatchSize: Int = 100

    override suspend fun generate(gameCount: Int): List<PositionGroup> =
        super.generate(gameCount).flatMap { positionGroup ->
            generateAllNextMovePositions(positionGroup.canonicalPosition).map { PositionGroup(it) } + positionGroup
        }

    private fun generateAllNextMovePositions(position: Position): List<Position> =
        potentialMoveSelector.selectPotentialMoves(position).mapNotNull {
            val game = Game(position)

            if (game.isNotEnded) {
                game.playNextMove(it)
                game.position
            } else {
                null
            }
        }
}