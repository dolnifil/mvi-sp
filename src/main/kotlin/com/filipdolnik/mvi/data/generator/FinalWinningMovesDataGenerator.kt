package com.filipdolnik.mvi.data.generator

import com.filipdolnik.mvi.ai.impl.MaxDistancePotentialMoveSelector
import com.filipdolnik.mvi.ai.position.CurrentPositionAi
import com.filipdolnik.mvi.data.PositionGroup
import com.filipdolnik.mvi.data.generator.internal.BaseDataGenerator
import com.filipdolnik.mvi.data.generator.internal.PotentialMoveDataGenerator
import com.filipdolnik.mvi.domain.Position

class FinalWinningMovesDataGenerator: PotentialMoveDataGenerator(MaxDistancePotentialMoveSelector(2)) {

    override val recommendedBatchSize: Int = 5000

    private val currentPositionAi = CurrentPositionAi()

    override suspend fun selectPositions(positionsInGameOrder: List<Position>): List<Position> =
        positionsInGameOrder.dropLast(1).takeLast(10).filter {
            currentPositionAi.evaluatePosition(it).isLosing
        }
}