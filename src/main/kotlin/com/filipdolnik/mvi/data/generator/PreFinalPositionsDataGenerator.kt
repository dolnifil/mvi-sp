package com.filipdolnik.mvi.data.generator

import com.filipdolnik.mvi.ai.Ai
import com.filipdolnik.mvi.ai.position.CurrentPositionAi
import com.filipdolnik.mvi.data.generator.internal.BaseDataGenerator
import com.filipdolnik.mvi.domain.Grid
import com.filipdolnik.mvi.domain.Position

class PreFinalPositionsDataGenerator: BaseDataGenerator() {

    override val recommendedBatchSize: Int = 50000

    override val targetDataSetSize: Int = 20000

    override suspend fun selectPositions(positionsInGameOrder: List<Position>): List<Position> =
        positionsInGameOrder.takeLast(2).take(1).filter { it.moveCount < Grid.SIZE.linearSize - 1 }

    override fun createPlayerXAi(): Ai = CurrentPositionAi()

    override fun createPlayerOAi(): Ai = CurrentPositionAi()
}