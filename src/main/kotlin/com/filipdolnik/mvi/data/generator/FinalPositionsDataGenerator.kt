package com.filipdolnik.mvi.data.generator

import com.filipdolnik.mvi.ai.Ai
import com.filipdolnik.mvi.ai.position.CurrentPositionAi
import com.filipdolnik.mvi.data.generator.internal.BaseDataGenerator
import com.filipdolnik.mvi.domain.Grid
import com.filipdolnik.mvi.domain.Position

class FinalPositionsDataGenerator: BaseDataGenerator() {

    override val recommendedBatchSize: Int = 25000

    override val targetDataSetSize: Int = 20000

    override suspend fun selectPositions(positionsInGameOrder: List<Position>): List<Position> =
        positionsInGameOrder.takeLast(1).filter { it.moveCount < Grid.SIZE.linearSize }

    override fun createPlayerXAi(): Ai = CurrentPositionAi()

    override fun createPlayerOAi(): Ai = CurrentPositionAi()
}