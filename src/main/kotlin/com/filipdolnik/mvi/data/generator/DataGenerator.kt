package com.filipdolnik.mvi.data.generator

import com.filipdolnik.mvi.data.PositionGroup
import com.filipdolnik.mvi.domain.Player

interface DataGenerator {

    val recommendedBatchSize: Int

    val targetDataSetSize: Int

    val supportedTypes: List<Player>

    suspend fun generate(gameCount: Int): List<PositionGroup>
}