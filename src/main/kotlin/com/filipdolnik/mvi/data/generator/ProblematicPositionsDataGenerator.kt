package com.filipdolnik.mvi.data.generator

import com.filipdolnik.mvi.ai.Ai
import com.filipdolnik.mvi.ai.position.CurrentPositionAi
import com.filipdolnik.mvi.data.generator.internal.BaseDataGenerator
import com.filipdolnik.mvi.domain.Player
import com.filipdolnik.mvi.domain.Position

class ProblematicPositionsDataGenerator(private val type: Player, private val ai: Ai): BaseDataGenerator() {

    override val recommendedBatchSize: Int = 1000

    override val targetDataSetSize: Int = 2000

    override val supportedTypes: List<Player> = listOf(type)

    override suspend fun selectPositions(positionsInGameOrder: List<Position>): List<Position> =
        if (positionsInGameOrder.last().currentPlayer == type.opponent) positionsInGameOrder else emptyList()

    override fun createPlayerXAi(): Ai = if (type.opponent == Player.X) ai else CurrentPositionAi()

    override fun createPlayerOAi(): Ai = if (type.opponent == Player.O) ai else CurrentPositionAi()
}