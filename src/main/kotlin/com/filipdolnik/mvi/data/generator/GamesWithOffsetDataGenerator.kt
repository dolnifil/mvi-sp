package com.filipdolnik.mvi.data.generator

import com.filipdolnik.mvi.data.generator.internal.BaseDataGenerator
import com.filipdolnik.mvi.domain.Coordinates
import com.filipdolnik.mvi.domain.Field
import com.filipdolnik.mvi.domain.Position

class GamesWithOffsetDataGenerator: BaseDataGenerator() {

    override suspend fun selectPositions(positionsInGameOrder: List<Position>): List<Position> = positionsInGameOrder

    override fun getInitialPosition(): Position = Position().also {
        val firstMove = Coordinates.fromValid(
            (3 until 12).random(),
            (3 until 12).random()
        )

        it[firstMove] = Field.X
    }
}