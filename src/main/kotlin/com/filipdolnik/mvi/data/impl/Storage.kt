package com.filipdolnik.mvi.data.impl

import com.filipdolnik.mvi.data.PositionGroupWithEvaluation
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.FileInputStream
import java.io.FileOutputStream
import java.nio.file.Paths
import java.util.zip.DeflaterOutputStream
import java.util.zip.InflaterInputStream

class Storage(private val name: String) {

    private val dataCodec = DataCodec()

    suspend fun load(): List<PositionGroupWithEvaluation> {
        if (!Paths.get(fileName(name)).toFile().exists()) {
            return emptyList()
        }

        @Suppress("BlockingMethodInNonBlockingContext")
        val byteArray = withContext(Dispatchers.IO) {
            val inputStream = FileInputStream(fileName(name))

            InflaterInputStream(inputStream).use {
                it.readAllBytes()
            }
        }

        return dataCodec.decompress(byteArray)
    }

    suspend fun save(data: List<PositionGroupWithEvaluation>) {
        val byteArray = dataCodec.compress(data)

        @Suppress("BlockingMethodInNonBlockingContext")
        withContext(Dispatchers.IO) {
            val outputStream = FileOutputStream(fileName(name))

            DeflaterOutputStream(outputStream).use {
                it.write(byteArray)
            }
        }
    }

    companion object {

        private fun fileName(name: String) = "src/main/resources/data/$name.bin"
    }
}