package com.filipdolnik.mvi.data.impl

import com.filipdolnik.mvi.data.PositionGroup
import com.filipdolnik.mvi.data.PositionGroupWithEvaluation
import com.filipdolnik.mvi.data.PositionWithEvaluation
import com.filipdolnik.mvi.domain.Coordinates
import com.filipdolnik.mvi.domain.Field
import com.filipdolnik.mvi.domain.Position
import com.filipdolnik.mvi.util.parallelMap
import kotlin.math.roundToInt

@OptIn(ExperimentalUnsignedTypes::class)
class DataCodec {

    suspend fun compress(data: List<PositionGroupWithEvaluation>): ByteArray {
        val entries = data.sortedBy { it.positionGroup.moveCount }

        var result = ByteArray(0)

        val dictionary = Dictionary()
        entries.forEach {
            result += compressNext(it, dictionary)
        }

        return result
    }

    private suspend fun compressNext(
        positionGroupWithEvaluation: PositionGroupWithEvaluation,
        dictionary: Dictionary
    ): ByteArray {
        val positionGroup = positionGroupWithEvaluation.positionGroup

        val bestParentIndex = dictionary.indexOfBestParent(positionGroup)
        val bestParent = dictionary[bestParentIndex]

        val rotatedPosition = positionGroup.allSymmetricPositions.first { bestParent.isPastOf(it) }
        dictionary.put(rotatedPosition, bestParentIndex)

        val missingMoves = bestParent.missingMoves(rotatedPosition)

        val bytes = ArrayList<UByte>()

        bytes.add((bestParentIndex % 256).toUByte())
        bytes.add(((bestParentIndex / 256) % 256).toUByte())
        bytes.add((bestParentIndex / 256 / 256).toUByte())

        bytes.add(missingMoves.size.toUByte())
        reorderMissingMoves(rotatedPosition, missingMoves).forEach {
            bytes.add(it.linearIndex.toUByte())
        }

        bytes.add((positionGroupWithEvaluation.evaluation * 100).roundToInt().toUByte())

        return bytes.toUByteArray().toByteArray()
    }

    private fun reorderMissingMoves(position: Position, missingMoves: List<Pair<Coordinates, Field>>): List<Coordinates> {
        val playerXMoves = missingMoves.filter { it.second == Field.X }
        val playerOMoves = missingMoves.filter { it.second == Field.O }

        val result = ArrayList<Coordinates>()

        val moveIndex = position.moveCount - missingMoves.size

        var playerXIndex = 0
        var playerOIndex = 0
        for (i in moveIndex until position.moveCount) {
            val moveWithField = if (i % 2 == 0) playerXMoves[playerXIndex++] else playerOMoves[playerOIndex++]

            result.add(moveWithField.first)
        }

        return result
    }

    fun decompress(data: ByteArray): List<PositionGroupWithEvaluation> {
        val uData = data.toUByteArray()

        var nextByte = 0

        val dictionary = ArrayList<Position>()
        val result = ArrayList<PositionWithEvaluation>()

        while (nextByte < data.size) {
            val entry = decompressNext(uData, nextByte, dictionary)

            dictionary.add(entry.positionWithEvaluation.position)
            result.add(entry.positionWithEvaluation)
            nextByte = entry.nextByte
        }

        return result.map { PositionGroupWithEvaluation(PositionGroup(it.position), it.evaluation) }
    }

    private fun decompressNext(data: UByteArray, startIndex: Int, dictionary: List<Position>): DecompressedEntry {
        var nextByte = startIndex

        val parentIndex = (data[nextByte++] + data[nextByte++] * 256u + data[nextByte++] * 256u * 256u).toInt()
        val moveCount = data[nextByte++].toInt()

        val moves = (0 until moveCount).map { Coordinates(data[nextByte++].toInt()) }

        val parentPosition = if (parentIndex != 0) dictionary[parentIndex - 1] else Position()
        val position = Position(parentPosition, moves)

        val evaluation = data[nextByte++].toInt() / 100.0

        return DecompressedEntry(
            PositionWithEvaluation(position, evaluation),
            nextByte
        )
    }

    private operator fun Position.Companion.invoke(basePosition: Position, moves: List<Coordinates>): Position {
        val position = basePosition.clone()

        moves.forEach {
            position[it] = position.currentPlayer.field
        }

        return position
    }

    private class Dictionary {

        private val entries = ArrayList<Position>()
        private val entriesToIndex = HashMap<Position, Int>()
        private val rootEntries = ArrayList<Position>()
        private val childrenByParent = HashMap<Position, ArrayList<Position>>()

        operator fun get(index: Int): Position = if (index != 0) entries[index - 1] else Position()

        suspend fun indexOfBestParent(positionGroup: PositionGroup): Int {
            var bestParentIndex = 0
            var bestParentMoveCount = 0
            var potentialParents: List<Position> = rootEntries

            while (potentialParents.isNotEmpty()) {
                val parents = if (potentialParents.size > 100) {
                    potentialParents.parallelMap { if (isParent(positionGroup, it)) it else null }.filterNotNull()
                } else {
                    potentialParents.filter { isParent(positionGroup, it) }
                }

                parents
                    .map {
                        val entryIndex = entriesToIndex[it] ?: error("Entry $it is not indexed in entriesToIndex.")

                        entryIndex to it
                    }
                    .filter { it.first < 256 * 256 * 256 }
                    .maxByOrNull { it.second.moveCount }
                    ?.let { (newBestParentIndex, newBestParent) ->
                        if (newBestParent.moveCount > bestParentMoveCount) {
                            bestParentIndex = newBestParentIndex
                            bestParentMoveCount = newBestParent.moveCount
                        }
                    }

                potentialParents = parents.flatMap {
                    childrenByParent[it] ?: emptyList()
                }
            }

            return bestParentIndex
        }

        private fun isParent(positionGroup: PositionGroup, potentialParent: Position): Boolean =
            positionGroup.allSymmetricPositions.any { potentialParent.isPastOf(it) }

        fun put(position: Position, parentIndex: Int) {
            entries.add(position)
            entriesToIndex[position] = entries.size

            if (parentIndex == 0 && rootEntries.size < 5000) {
                rootEntries.add(position)
            } else {
                val parentPosition = get(parentIndex)
                val children = childrenByParent.getOrPut(parentPosition) { ArrayList() }
                children.add(position)
            }
        }
    }

    private data class DecompressedEntry(
        val positionWithEvaluation: PositionWithEvaluation,
        val nextByte: Int
    )
}