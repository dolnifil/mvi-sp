package com.filipdolnik.mvi.domain.error

import com.filipdolnik.mvi.domain.GameResult

data class GameOver(private val gameResult: GameResult): DomainError("Game is over. Result: $gameResult.")
