package com.filipdolnik.mvi.domain.error

import com.filipdolnik.mvi.domain.Coordinates
import com.filipdolnik.mvi.domain.Field

data class IllegalMove(private val coordinates: Coordinates, private val field: Field):
    DomainError("Cannot move on field $coordinates as it is $field.")
