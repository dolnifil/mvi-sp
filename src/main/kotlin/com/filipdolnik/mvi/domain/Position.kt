package com.filipdolnik.mvi.domain

class Position private constructor(
    moveCount: Int,
    private val fields: Array<Field>
) {

    var moveCount: Int = moveCount
        private set

    val currentPlayer: Player
        get() = if (moveCount % 2 == 0) Player.X else Player.O

    constructor(): this(
        0,
        Array(Grid.SIZE.linearSize) { Field.Empty }
    )

    operator fun get(coordinates: Coordinates): Field = fields[coordinates.linearIndex]

    operator fun set(coordinates: Coordinates, value: Field) {
        when {
            this[coordinates] == Field.Empty && value != Field.Empty -> moveCount++
            this[coordinates] != Field.Empty && value == Field.Empty -> moveCount--
        }

        fields[coordinates.linearIndex] = value
    }

    fun containsMove(coordinates: Coordinates): Boolean = this[coordinates] != Field.Empty

    fun isPastOf(position: Position): Boolean = Grid.allCoordinates.all {
        val field = this[it]

        field == Field.Empty || field == position[it]
    }

    fun missingMoves(position: Position): List<Pair<Coordinates, Field>> =
        Grid.allCoordinates
            .filter { coordinates ->
                if (this[coordinates] != position[coordinates]) {
                    if (this[coordinates] == Field.Empty) {
                        true
                    } else {
                        throw IllegalArgumentException("$this is not a past position of $position.")
                    }
                } else {
                    false
                }
            }
            .map { it to position[it] }

    fun clone(): Position = Position(moveCount, fields.clone())

    companion object {
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Position

        if (!fields.contentEquals(other.fields)) return false

        return true
    }

    override fun hashCode(): Int {
        return fields.contentHashCode()
    }

    override fun toString(): String {
        return "Position(fields=${fields.contentToString()})"
    }
}