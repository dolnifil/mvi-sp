package com.filipdolnik.mvi.domain

enum class Field {
    Empty,
    X,
    O
}