package com.filipdolnik.mvi.domain

import com.filipdolnik.mvi.domain.error.GameOver
import com.filipdolnik.mvi.domain.error.IllegalMove

class Game(
    initialPosition: Position? = null
) {

    var result: GameResult? = null
        private set

    val currentPlayer: Player
        get() = position.currentPlayer

    val isNotEnded: Boolean
        get() = result == null

    val isEnded: Boolean
        get() = !isNotEnded

    val latestMoveCoordinates: Coordinates?
        get() = moveHistory.lastOrNull()

    val moveCount: Int
        get() = position.moveCount

    val position: Position = Position()

    private val moveHistory = ArrayList<Coordinates>()

    init {
        if (initialPosition != null) {
            Grid.allCoordinates.forEach {
                position[it] = initialPosition[it]
            }

            Grid.allCoordinates.forEach {
                if (fieldAt(it) != Field.Empty) {
                    checkWinner(it)
                }
            }

            checkDraw()
        }
    }

    fun playNextMove(coordinates: Coordinates) {
        result?.let { throw GameOver(it) }
        if (!isValidMove(coordinates)) {
            throw IllegalMove(coordinates, fieldAt(coordinates))
        }

        makeMove(coordinates)

        checkGameOver(coordinates)
    }

    private fun isValidMove(coordinates: Coordinates): Boolean = !position.containsMove(coordinates)

    private fun makeMove(coordinates: Coordinates) {
        position[coordinates] = currentPlayer.field

        moveHistory.add(coordinates)
    }

    private fun checkGameOver(latestMoveCoordinates: Coordinates) {
        if (isNotEnded) {
            checkWinner(latestMoveCoordinates)
        }

        if (isNotEnded) {
            checkDraw()
        }
    }

    private fun checkWinner(latestMoveCoordinates: Coordinates) {
        val currentField = fieldAt(latestMoveCoordinates)

        val isWinner = isWinningField(position, latestMoveCoordinates)

        if (isWinner) {
            result = when (currentField) {
                Field.X -> GameResult.X
                Field.O -> GameResult.O
                else -> throw IllegalArgumentException("$latestMoveCoordinates are not pointing to player move.")
            }
        }
    }

    private fun checkDraw() {
        if (position.moveCount >= Grid.SIZE.linearSize && result == null) {
            result = GameResult.Draw
        }
    }

    fun revertLatestMove() {
        result = null

        val latestMoveCoordinates = moveHistory.removeLastOrNull()

        latestMoveCoordinates?.let {
            position[it] = Field.Empty
        }
    }

    fun fieldAt(coordinates: Coordinates): Field = position[coordinates]

    companion object {

        const val TARGET_COUNT: Int = 5

        fun isWinningField(position: Position, coordinates: Coordinates): Boolean {
            val currentField = position[coordinates]

            return Grid.allNeighborhoodPathsWithCenter(coordinates).any { path ->
                var count = 1
                for (neighbour in path.rightPath) {
                    if (position[neighbour] == currentField) {
                        count++
                    } else {
                        break
                    }
                }
                for (neighbour in path.reversedLeftPart) {
                    if (position[neighbour] == currentField) {
                        count++
                    } else {
                        break
                    }
                }

                count == TARGET_COUNT
            }
        }
    }
}