package com.filipdolnik.mvi.domain

object Grid {

    val SIZE: Size = Size(15, 15)

    val allCoordinates: List<Coordinates> = (0 until SIZE.linearSize).map { Coordinates(it) }

    private val neighborhoodRange = (-Game.TARGET_COUNT..Game.TARGET_COUNT)

    private val neighborhoodPaths =
        allCoordinates.map { coordinates ->
            listOf(
                neighborhoodRange.mapNotNull { Coordinates(coordinates.x + it, coordinates.y) },
                neighborhoodRange.mapNotNull { Coordinates(coordinates.x, coordinates.y + it) },
                neighborhoodRange.mapNotNull { Coordinates(coordinates.x + it, coordinates.y + it) },
                neighborhoodRange.mapNotNull { Coordinates(coordinates.x + it, coordinates.y - it) }
            ).map { Path(it, coordinates) }
        }

    fun allNeighborhoodPathsWithCenter(coordinates: Coordinates): List<Path> = neighborhoodPaths[coordinates.linearIndex]

    class Path(coordinatesList: List<Coordinates>, center: Coordinates) {

        val reversedLeftPart: List<Coordinates> = coordinatesList.takeWhile { it != center }.reversed()

        val rightPath: List<Coordinates> = coordinatesList.takeLastWhile { it != center }
    }
}