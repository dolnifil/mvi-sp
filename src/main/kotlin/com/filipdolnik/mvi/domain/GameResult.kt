package com.filipdolnik.mvi.domain

enum class GameResult {
    O, X, Draw
}