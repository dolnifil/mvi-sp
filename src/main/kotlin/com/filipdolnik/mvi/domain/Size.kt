package com.filipdolnik.mvi.domain

data class Size(val width: Int, val height: Int) {

    val linearSize: Int = width * height
}