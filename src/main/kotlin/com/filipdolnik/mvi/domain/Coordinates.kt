package com.filipdolnik.mvi.domain

inline class Coordinates(val linearIndex: Int): Comparable<Coordinates> {

    val x: Int
        get() = linearIndex % Grid.SIZE.width

    val y: Int
        get() = linearIndex / Grid.SIZE.width

    companion object {

        private val widthRange = 0 until Grid.SIZE.width
        private val heightRange = 0 until Grid.SIZE.height

        operator fun invoke(x: Int, y: Int): Coordinates? {
            if (x !in widthRange || y !in heightRange) {
                return null
            }

            val linearIndex = y * Grid.SIZE.width + x
            return Coordinates(linearIndex)
        }

        fun fromValid(x: Int, y: Int): Coordinates =
            Coordinates(x, y) ?: throw IndexOutOfBoundsException("Coordinates $x, $y are not valid.")
    }

    override fun compareTo(other: Coordinates): Int =
        linearIndex.compareTo(other.linearIndex)

    override fun toString(): String {
        return "Coordinates(x=$x, y=$y)"
    }
}