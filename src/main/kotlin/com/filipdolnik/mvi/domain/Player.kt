package com.filipdolnik.mvi.domain

enum class Player {

    O, X;

    val field: Field by lazy {
        when (this) {
            O -> Field.O
            X -> Field.X
        }
    }

    val opponent: Player by lazy {
        when (this) {
            X -> O
            O -> X
        }
    }
}