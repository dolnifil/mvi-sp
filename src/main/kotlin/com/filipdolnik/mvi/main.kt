package com.filipdolnik.mvi

import com.filipdolnik.mvi.app.AiDebuggingApplication
import com.filipdolnik.mvi.app.AivAiInspectApplication
import com.filipdolnik.mvi.app.AivAiStatisticsApplication
import com.filipdolnik.mvi.app.BenchmarkApplication
import com.filipdolnik.mvi.app.ClassicalAiTrainingApplication
import com.filipdolnik.mvi.app.DataGeneratorApplication
import com.filipdolnik.mvi.app.PvAiApplication
import com.filipdolnik.mvi.app.PvPApplication
import com.filipdolnik.mvi.app.ReinforcementAiTrainingApplication
import com.filipdolnik.mvi.app.TestingApplication
import com.filipdolnik.mvi.util.StopWatch
import kotlinx.coroutines.runBlocking

fun main(args: Array<String>) {
    while (true) {
        val applications = listOf(
            PvPApplication(),
            PvAiApplication(),
            AivAiInspectApplication(),
            AivAiStatisticsApplication(),
            ClassicalAiTrainingApplication(),
            ReinforcementAiTrainingApplication(),
            DataGeneratorApplication(),
            AiDebuggingApplication(),
            BenchmarkApplication(),
            TestingApplication(),
            PvAiApplication(true)
        )

        println("0: Exit")
        applications.forEachIndexed { index, application ->
            println("${index + 1}: ${application.description}")
        }

        println()
        print("Select option: ")

        val input = readLine()
        val option = input?.toIntOrNull()
        if (option == 0) {
            return
        }

        val application = option?.let { applications.getOrNull(it - 1) }
        if (application == null) {
            println("Wrong option.")
            continue
        }

        runBlocking {
            application.start()
        }

        println()

        StopWatch.printAllTimes()
        StopWatch.reset()

        println()
    }
}