package com.filipdolnik.mvi.app

interface Application {

    val description: String

    suspend fun start()
}