package com.filipdolnik.mvi.app

import com.filipdolnik.mvi.app.gamestate.DefaultGameStateDescription
import com.filipdolnik.mvi.domain.Coordinates
import com.filipdolnik.mvi.view.GameStateView
import com.filipdolnik.mvi.view.GameView

class PvPApplication: BaseInteractiveApplication() {

    override val description: String = "P vs P"

    override val gameView: GameView = GameView()

    override val gameStateDescription: GameStateView.GameStateDescription = DefaultGameStateDescription(game)

    override suspend fun playNextMove(coordinates: Coordinates) {
        game.playNextMove(coordinates)
    }

    override fun revertLatestMove() {
        game.revertLatestMove()
    }
}