package com.filipdolnik.mvi.app

import com.filipdolnik.mvi.ai.ml.PersistedNeuralNetworkAi
import com.filipdolnik.mvi.ai.ml.impl.PositionDataSetIterator
import com.filipdolnik.mvi.ai.position.CurrentPositionAi
import com.filipdolnik.mvi.data.DataSet
import com.filipdolnik.mvi.data.PositionWithEvaluation
import com.filipdolnik.mvi.domain.Player
import com.filipdolnik.mvi.util.parallelMap
import kotlin.math.roundToInt
import kotlin.system.measureTimeMillis

class BenchmarkApplication: Application {

    override val description: String = "Benchmark"

    override suspend fun start() {
        benchmarkAi()
    }

    private suspend fun benchmarkAi() {
        val dataSize: Int
        val time = measureTimeMillis {
//            dataSize = benchmarkPredictions(1000, 100000)
            dataSize = benchmarkTraining(1000, 100000)
        }

        val seconds = time / 1000.0
        println("Time: $seconds; Examples/sec: ${dataSize / seconds}")
    }

    private suspend fun benchmarkOther() {
        val time = measureTimeMillis {
            benchmarkDataSets()
        }

        val seconds = time / 1000.0
        println("Time: $seconds")
    }

    private suspend fun benchmarkPredictions(batchSize: Int, chunkSize: Int): Int {
        val dataSet = DataSet.games(Player.X)
        val data = dataSet.data.map { it.positionGroup.canonicalPosition }
        val dataSetIterators = data.chunked(chunkSize).map { PositionDataSetIterator.forPrediction(batchSize, it) }

        val network = PersistedNeuralNetworkAi.classicalAi().playerXAi

        dataSetIterators.forEach {
            network.output(it)
        }

        val dataSize = ((chunkSize / batchSize) * batchSize) * (data.size / chunkSize)

        println("Data size: $dataSize")
        println("Predictions: $batchSize/${chunkSize}")

        return dataSize
    }

    private suspend fun benchmarkTraining(batchSize: Int, chunkSize: Int): Int {
        val dataSet = DataSet.games(Player.X)
        val data = dataSet.data.map { PositionWithEvaluation(it.positionGroup.canonicalPosition, it.evaluation) }
        val dataSetIterators = data.chunked(chunkSize).map { PositionDataSetIterator.forTraining(batchSize, it) }

        val network = PersistedNeuralNetworkAi.classicalAi().playerXAi

        dataSetIterators.forEach {
            network.fit(it)
        }

        val dataSize = ((chunkSize / batchSize) * batchSize) * (data.size / chunkSize)

        println("Data size: $dataSize")
        println("Training: $batchSize/$chunkSize")

        return dataSize
    }

    private suspend fun benchmarkDataSets() {
        for (type in Player.values()) {
            val dataSet = DataSet.games(type)

            dataSet.data.parallelMap { (positionGroup, evaluation) ->
                val ai = CurrentPositionAi()

                val evaluations = positionGroup.allSymmetricPositions.map { ai.evaluatePosition(it) }
                val canonicalPositionEvaluation = evaluations.first()

                if ((canonicalPositionEvaluation.mergedEvaluation * 100).roundToInt() / 100.0 != evaluation) {
                    println(
                        "Error: ${positionGroup.canonicalPosition} is not evaluated correctly. " +
                            "Was ${evaluation}, now: ${canonicalPositionEvaluation.mergedEvaluation}"
                    )
                }

                if (evaluations.any { it != evaluations.first() }) {
                    println(
                        "Error: ${positionGroup.canonicalPosition} symmetrical positions are not evaluated correctly. $evaluations"
                    )
                }
            }
        }
    }
}