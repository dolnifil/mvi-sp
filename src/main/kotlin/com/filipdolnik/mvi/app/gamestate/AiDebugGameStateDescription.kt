package com.filipdolnik.mvi.app.gamestate

import com.filipdolnik.mvi.ai.PositionEvaluation
import com.filipdolnik.mvi.domain.Coordinates
import com.filipdolnik.mvi.domain.Field
import com.filipdolnik.mvi.domain.Game
import com.filipdolnik.mvi.view.GameStateView
import kotlin.math.roundToInt

class AiDebugGameStateDescription(private val game: Game, nextMovesEvaluation: List<Pair<Coordinates, PositionEvaluation>>):
    GameStateView.GameStateDescription {

    override val maxColumnWidth: Int = 5

    private val nextMovesEvaluation = nextMovesEvaluation.toMap()

    override fun contentAt(coordinates: Coordinates): String {
        val content = when (game.fieldAt(coordinates)) {
            Field.X -> "X"
            Field.O -> "O"
            else -> nextMovesEvaluation[coordinates]?.winningProbability?.let {
                String.format("%.2f", ((it * 100).roundToInt() / 100.0))
            } ?: "-"
        }

        return if (game.latestMoveCoordinates == coordinates) "!$content!" else content
    }
}