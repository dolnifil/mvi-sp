package com.filipdolnik.mvi.app.gamestate

import com.filipdolnik.mvi.domain.Coordinates
import com.filipdolnik.mvi.domain.Field
import com.filipdolnik.mvi.domain.Game
import com.filipdolnik.mvi.view.GameStateView

class DefaultGameStateDescription(private val game: Game): GameStateView.GameStateDescription {

    override val maxColumnWidth: Int = 3

    override fun contentAt(coordinates: Coordinates): String {
        val content = when (game.fieldAt(coordinates)) {
            Field.X -> "X"
            Field.O -> "O"
            else -> ""
        }

        return if (game.latestMoveCoordinates == coordinates) "!$content!" else content
    }
}