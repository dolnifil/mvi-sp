package com.filipdolnik.mvi.app

import com.filipdolnik.mvi.ai.AiFactory
import com.filipdolnik.mvi.ai.ml.NeuralNetworkAi
import com.filipdolnik.mvi.domain.Game
import com.filipdolnik.mvi.domain.GameResult
import com.filipdolnik.mvi.domain.Player
import com.filipdolnik.mvi.util.parallelMap
import kotlin.system.measureTimeMillis

abstract class BaseAivAiStatisticsApplication: Application {

    protected abstract val playerXFactory: AiFactory

    protected abstract val playerOFactory: AiFactory

    protected abstract val iterations: Int

    override suspend fun start() {
        NeuralNetworkAi.disableDebug()

        val statistics: List<GameStatistics>
        val time = measureTimeMillis {
            statistics = (0 until iterations)
                .parallelMap { playGame() }
        }

        val playerXWins = statistics.filter { it.result == GameResult.X }
        val playerOWins = statistics.filter { it.result == GameResult.O }

        val playerXWinCount = playerXWins.size
        val playerOWinCount = playerOWins.size
        val drawCount = statistics.count { it.result == GameResult.Draw }

        val averageMoveCount = statistics.map { it.moveCount }.average()
        val averagePlayerXWinMoveCount = if (playerXWins.isNotEmpty()) playerXWins.map { it.moveCount }.average() else 0.0
        val averagePlayerOWinMoveCount = if (playerOWins.isNotEmpty()) playerOWins.map { it.moveCount }.average() else 0.0

        println(
            "X: $playerXWinCount; O: $playerOWinCount; Draws: $drawCount; " +
                "Avg move count: $averageMoveCount; " +
                "Avg X wins move count: $averagePlayerXWinMoveCount; Avg O wins move count: $averagePlayerOWinMoveCount; " +
                "Time: ${time / 1000.0};"
        )
    }

    private suspend fun playGame(): GameStatistics {
        val game = Game()
        val playerX = playerXFactory()
        val playerO = playerOFactory()

        while (true) {
            val ai = when (game.currentPlayer) {
                Player.X -> playerX
                Player.O -> playerO
            }

            val nextAiMoveCoordinates = ai.suggestNextMove(game.position)

            game.playNextMove(nextAiMoveCoordinates)

            val gameResult = game.result
            if (gameResult != null) {
                return GameStatistics(gameResult, game.moveCount)
            }
        }
    }

    private data class GameStatistics(val result: GameResult, val moveCount: Int)
}