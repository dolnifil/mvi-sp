package com.filipdolnik.mvi.app

import ch.qos.logback.classic.Level
import com.filipdolnik.mvi.ai.Ai
import com.filipdolnik.mvi.ai.AiFactory
import com.filipdolnik.mvi.ai.ml.LoadedNeuralNetworkAi
import com.filipdolnik.mvi.ai.ml.NetworkBuilder
import com.filipdolnik.mvi.ai.ml.NeuralNetworkAi
import com.filipdolnik.mvi.ai.ml.PersistedNeuralNetworkAi
import com.filipdolnik.mvi.ai.ml.impl.ConvolutionalNetworkBuilder
import com.filipdolnik.mvi.ai.ml.impl.PositionDataSetIterator
import com.filipdolnik.mvi.ai.position.CurrentPositionAi
import com.filipdolnik.mvi.data.PositionGroup
import com.filipdolnik.mvi.data.PositionGroupWithEvaluation
import com.filipdolnik.mvi.data.PositionWithEvaluation
import com.filipdolnik.mvi.domain.Game
import com.filipdolnik.mvi.domain.Grid
import com.filipdolnik.mvi.domain.Player
import com.filipdolnik.mvi.domain.Position
import com.filipdolnik.mvi.util.parallelMap
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork
import org.deeplearning4j.optimize.listeners.PerformanceListener
import org.deeplearning4j.ui.api.UIServer
import org.deeplearning4j.ui.model.stats.StatsListener
import org.deeplearning4j.ui.model.stats.impl.DefaultStatsUpdateConfiguration
import org.deeplearning4j.ui.model.storage.InMemoryStatsStorage
import org.nd4j.linalg.activations.impl.ActivationSigmoid
import kotlin.math.pow

class ReinforcementAiTrainingApplication: Application {

    override val description: String = "Reinforcement Ai training"

    private val name = "reinforcement"
    private val firstEpochIndex = 0

    private val maxBatchSize = 1024
    private val epochLength = 400
    private val gamesPerIteration = 10
    private val epochs = 30
    private val networkBuilder = ConvolutionalNetworkBuilder()

    private val learningRate = 0.30

    private val explorationProbability = 0.1
    private val secondMoveExplorationProbability = 0.90
    private val teacherProbability = 0.0

    private val testingOpponent = CurrentPositionAi()

    override suspend fun start() {
        NeuralNetworkAi.enableDebug(Level.INFO)

        val (playerXStatsStorage, playerOStatsStorage) = startUIServer()
        val playerXStatsListener = StatsListener(playerXStatsStorage, DefaultStatsUpdateConfiguration.DEFAULT_REPORTING_FREQUENCY, "X")
        val playerOStatsListener = StatsListener(playerOStatsStorage, DefaultStatsUpdateConfiguration.DEFAULT_REPORTING_FREQUENCY, "O")

        val playerXModel = loadModelOrCreateNew(networkBuilder, Player.X)
        val playerOModel = loadModelOrCreateNew(networkBuilder, Player.O)

        playerXModel.setListeners(playerXStatsListener, PerformanceListener(100, true, true))
        playerOModel.setListeners(playerOStatsListener, PerformanceListener(100, true, true))

        trainModels(playerXModel, playerOModel)
    }

    private fun startUIServer(): Pair<InMemoryStatsStorage, InMemoryStatsStorage> {
        val playerXStatsStorage = InMemoryStatsStorage()
        val playerOStatsStorage = InMemoryStatsStorage()

        UIServer.getInstance(true) {
            when (it) {
                "X" -> playerXStatsStorage
                "O" -> playerOStatsStorage
                else -> null
            }
        }

        return playerXStatsStorage to playerOStatsStorage
    }

    private fun loadModelOrCreateNew(networkBuilder: NetworkBuilder, type: Player): MultiLayerNetwork {
        val file = PersistedNeuralNetworkAi.networkFile(name, type, firstEpochIndex - 1)
        val model = if (file.exists()) {
            MultiLayerNetwork.load(file, true)
        } else {
            networkBuilder.build()
        }

        println(model.summary())

        return model
    }

    private suspend fun trainModels(playerXModel: MultiLayerNetwork, playerOModel: MultiLayerNetwork) {
        //        println("Pre training results:")
        //        evaluateModels(playerXModel, playerOModel)

        repeat(epochs) {
            runTrainingEpoch(playerXModel, playerOModel, firstEpochIndex + it)
        }
    }

    private suspend fun runTrainingEpoch(
        playerXModel: MultiLayerNetwork,
        playerOModel: MultiLayerNetwork,
        epochIndex: Int
    ) {
        repeat(epochLength) {
            runTrainingIteration(playerXModel, playerOModel)
        }

        save(playerXModel, Player.X, epochIndex)
        save(playerOModel, Player.O, epochIndex)

        println("Epoch $epochIndex ended. Results:")
        evaluateModels(playerXModel, playerOModel)
    }

    private suspend fun runTrainingIteration(playerXModel: MultiLayerNetwork, playerOModel: MultiLayerNetwork) {
        val trainedAi = LoadedNeuralNetworkAi(playerXModel, playerOModel)
        val positionsWithEvaluation = (0 until gamesPerIteration)
            .parallelMap {
                val movesDescription = generateMovesFromGames(trainedAi)
                estimatePositionsEvaluation(movesDescription)
            }
            .flatten()

        val playerXPositions = positionsWithEvaluation.filter { it.position.currentPlayer == Player.X }
        val playerOPositions = positionsWithEvaluation.filter { it.position.currentPlayer == Player.O }

        fitModel(playerXModel, playerXPositions)
        fitModel(playerOModel, playerOPositions)
    }

    private suspend fun generateMovesFromGames(trainedAi: NeuralNetworkAi): List<MoveDescription> {
        fun selectAi(trainedAi: NeuralNetworkAi): Ai = if (Math.random() < teacherProbability) CurrentPositionAi() else trainedAi

        val playerX = selectAi(trainedAi)
        val playerO = selectAi(trainedAi)

        val movesDescription = playGame(playerX, playerO)

        return movesDescription.map {
            if ((it.targetPosition.currentPlayer == Player.X && playerO == trainedAi) ||
                (it.targetPosition.currentPlayer == Player.O && playerX == trainedAi)
            ) {
                it
            } else {
                it.copy(currentTargetPositionEvaluation = trainedAi.evaluatePosition(it.targetPosition).mergedEvaluation)
            }
        }
    }

    private suspend fun playGame(playerX: Ai, playerY: Ai): List<MoveDescription> {
        val moves = ArrayList<MoveDescription>()
        val game = Game()

        while (game.isNotEnded) {
            val currentAi = if (game.currentPlayer == Player.X) playerX else playerY

            val isExploratoryMove = Math.random() < explorationProbability

            val (move, moveEvaluation) = if (isExploratoryMove) {
                val possibleMoves = currentAi.evaluateAllNextMoves(game.position)

                if (Math.random() < secondMoveExplorationProbability) {
                    val sortedMoves = possibleMoves.sortedByDescending { it.second }
                    sortedMoves.getOrNull(2) ?: sortedMoves.first()
                } else {
                    possibleMoves.random()
                }
            } else {
                currentAi.suggestNextMoveWithEvaluation(game.position)
            }

            game.playNextMove(move)

            val position = game.position.clone()
            val evaluation = moveEvaluation.reversePlayerPerspective().mergedEvaluation

            val moveDescription = MoveDescription(position, evaluation, isExploratoryMove)
            moves.add(moveDescription)
        }

        return moves
    }

    private fun estimatePositionsEvaluation(movesDescription: List<MoveDescription>): List<PositionWithEvaluation> {
        val winner = movesDescription.last().targetPosition.currentPlayer.opponent
        val looser = winner.opponent

        val winnerMovesDescription = movesDescription.filter { it.targetPosition.currentPlayer == winner }
        val looserMovesDescription = movesDescription.filter { it.targetPosition.currentPlayer == looser }

        val winnerFinalMoveEvaluation = if (movesDescription.size < Grid.SIZE.linearSize) 1.0 else 0.35
        val looserFinalMoveEvaluation = if (movesDescription.size < Grid.SIZE.linearSize) 0.0 else 0.35

        return adjustSinglePlayerEvaluation(winnerMovesDescription, winnerFinalMoveEvaluation) +
            adjustSinglePlayerEvaluation(looserMovesDescription, looserFinalMoveEvaluation)
    }

    private fun adjustSinglePlayerEvaluation(
        movesDescription: List<MoveDescription>,
        finalMoveTargetEvaluation: Double
    ): List<PositionWithEvaluation> {
        val lastExplorationMoveIndex = movesDescription.indexOfLast { it.isExploration }

        return movesDescription.mapIndexedNotNull { index, (position, oldEvaluation) ->
            val targetEvaluation = calculateTargetEvaluation(index, movesDescription.lastIndex, finalMoveTargetEvaluation)

            val newEvaluation = ((1 - learningRate) * oldEvaluation + learningRate * targetEvaluation).coerceIn(0.0, 1.0)

            if (index < lastExplorationMoveIndex && newEvaluation < oldEvaluation) {
                null
            } else {
                PositionWithEvaluation(position, newEvaluation)
            }
        }
    }

    private fun calculateTargetEvaluation(index: Int, maxIndex: Int, finalMoveTargetEvaluation: Double): Double {
        val baseOffset = 0.5
        val maxDifference = finalMoveTargetEvaluation - baseOffset
        val minDifference = if (maxDifference > 0) 0.01 else -0.01

        val relativeDifference = (index / maxIndex.toDouble()).pow(1.5)

        return baseOffset + minDifference + (maxDifference - minDifference) * relativeDifference
    }

    private fun fitModel(model: MultiLayerNetwork, positionsWithEvaluation: List<PositionWithEvaluation>) {
        val positionGroupsWithEvaluation =
            positionsWithEvaluation.map { PositionGroupWithEvaluation(PositionGroup(it.position), it.evaluation) }
        val batchSize = (positionsWithEvaluation.size * 8).coerceAtMost(maxBatchSize)

        val trainingDataSet = PositionDataSetIterator.forTraining(batchSize, positionGroupsWithEvaluation)

        model.fit(trainingDataSet)
    }

    private suspend fun evaluateModels(playerXModel: MultiLayerNetwork, playerOModel: MultiLayerNetwork) {
        val ai = LoadedNeuralNetworkAi(playerXModel, playerOModel)

        print("Player X vs Player O: ")
        AiEvaluationApplication(ai, ai).start()

        print("Player X vs Testing: ")
        AiEvaluationApplication(ai, testingOpponent).start()

        print("Testing vs Player O: ")
        AiEvaluationApplication(testingOpponent, ai).start()
    }

    private suspend fun save(model: MultiLayerNetwork, type: Player, epochIndex: Int) {
        val file = PersistedNeuralNetworkAi.networkFile(name, type, epochIndex)

        withContext(Dispatchers.IO) {
            @Suppress("BlockingMethodInNonBlockingContext")
            model.save(file, true)
        }
    }

    private data class MoveDescription(
        val targetPosition: Position,
        val currentTargetPositionEvaluation: Double,
        val isExploration: Boolean
    )

    private class AiEvaluationApplication(playerXAi: Ai, playerOAi: Ai): BaseAivAiStatisticsApplication() {

        override val description: String = ""

        override val playerXFactory: AiFactory = { playerXAi }
        override val playerOFactory: AiFactory = { playerOAi }

        override val iterations: Int = 100

        override suspend fun start() {
            super.start()

            NeuralNetworkAi.enableDebug(Level.INFO)
        }
    }
}