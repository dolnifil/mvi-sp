package com.filipdolnik.mvi.app

import com.filipdolnik.mvi.ai.AiFactory
import com.filipdolnik.mvi.view.AiSelectionView

class AivAiStatisticsApplication: BaseAivAiStatisticsApplication() {

    override val description: String = "Ai v Ai statistics"

    override lateinit var playerXFactory: AiFactory
        private set

    override lateinit var playerOFactory: AiFactory
        private set

    override val iterations: Int = 100

    override suspend fun start() {
        playerXFactory = AiSelectionView.selectAi()
        playerOFactory = AiSelectionView.selectAi()

        super.start()
    }
}