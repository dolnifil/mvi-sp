package com.filipdolnik.mvi.app

import com.filipdolnik.mvi.domain.Coordinates
import com.filipdolnik.mvi.domain.Game
import com.filipdolnik.mvi.domain.error.DomainError
import com.filipdolnik.mvi.view.GameStateView
import com.filipdolnik.mvi.view.GameView

abstract class BaseInteractiveApplication: Application {

    protected val game: Game = Game()

    protected abstract val gameView: GameView

    protected abstract val gameStateDescription: GameStateView.GameStateDescription

    override suspend fun start() {
        gameView.printHelp()

        while (true) {
            try {
                playGame()

                when (handleGameEnd()) {
                    GameView.EndedGameAction.Revert -> revertLatestMove()
                    GameView.EndedGameAction.End -> break
                }
            } catch (e: DomainError) {
                gameView.printError(e)
            }
        }
    }

    private suspend fun playGame() {
        while (game.isNotEnded) {
            gameView.printGameState(gameStateDescription)

            when (val action = gameView.requestNextAction(game.currentPlayer)) {
                is GameView.Action.Move -> playNextMove(action.coordinates)
                GameView.Action.Revert -> revertLatestMove()
                GameView.Action.End -> break
            }
        }
    }

    private fun handleGameEnd(): GameView.EndedGameAction {
        gameView.printGameState(gameStateDescription)

        val gameResult = game.result
        return if (gameResult != null) {
            gameView.printGameResult(gameResult, game.moveCount)

            gameView.requestEndedGameAction()
        } else {
            gameView.printGameEndedWithoutResult()

            GameView.EndedGameAction.End
        }
    }

    protected abstract suspend fun playNextMove(coordinates: Coordinates)

    protected abstract fun revertLatestMove()
}