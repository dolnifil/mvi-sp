package com.filipdolnik.mvi.app

import com.filipdolnik.mvi.ai.Ai
import com.filipdolnik.mvi.ai.ml.NeuralNetworkAi
import com.filipdolnik.mvi.app.gamestate.AiDebugGameStateDescription
import com.filipdolnik.mvi.app.gamestate.DefaultGameStateDescription
import com.filipdolnik.mvi.domain.Coordinates
import com.filipdolnik.mvi.view.AiSelectionView
import com.filipdolnik.mvi.view.GameStateView
import com.filipdolnik.mvi.view.PlayerVsAiGameView

class PvAiApplication(private val debug: Boolean = false): BaseInteractiveApplication() {

    override val description: String = "P vs Ai" + if (debug) " (debug)" else ""

    override val gameView: PlayerVsAiGameView = PlayerVsAiGameView()

    override var gameStateDescription: GameStateView.GameStateDescription = DefaultGameStateDescription(game)
        private set

    private lateinit var ai: Ai

    override suspend fun start() {
        NeuralNetworkAi.disableDebug()

        ai = AiSelectionView.selectAi()()

        val playerStarts = gameView.decideIfPlayerStarts()
        if (!playerStarts) {
            playAiMove()
        }

        super.start()
    }

    override suspend fun playNextMove(coordinates: Coordinates) {
        game.playNextMove(coordinates)

        playAiMove()
    }

    private suspend fun playAiMove() {
        if (game.isNotEnded) {
            if (debug) {
                val nextMovesEvaluation = ai.evaluateAllNextMoves(game.position)
                gameStateDescription = AiDebugGameStateDescription(game, nextMovesEvaluation)
            }

            val nextAiMoveCoordinates = ai.suggestNextMove(game.position)

            game.playNextMove(nextAiMoveCoordinates)
        }
    }

    override fun revertLatestMove() {
        game.revertLatestMove()
        game.revertLatestMove()
    }
}