package com.filipdolnik.mvi.app

import com.filipdolnik.mvi.ai.ml.PersistedNeuralNetworkAi
import com.filipdolnik.mvi.ai.position.CurrentPositionAi
import com.filipdolnik.mvi.data.DataSet
import com.filipdolnik.mvi.data.PositionGroup
import com.filipdolnik.mvi.data.PositionGroupWithEvaluation
import com.filipdolnik.mvi.data.generator.DataGenerator
import com.filipdolnik.mvi.data.generator.EndGameWithMovesDataGenerator
import com.filipdolnik.mvi.data.generator.FinalLoosingMovesDataGenerator
import com.filipdolnik.mvi.data.generator.FinalPositionsDataGenerator
import com.filipdolnik.mvi.data.generator.FinalWinningMovesDataGenerator
import com.filipdolnik.mvi.data.generator.GamesDataGenerator
import com.filipdolnik.mvi.data.generator.GamesWithOffsetDataGenerator
import com.filipdolnik.mvi.data.generator.GamesWithPotentialMovesDataGenerator
import com.filipdolnik.mvi.data.generator.PreFinalPositionsDataGenerator
import com.filipdolnik.mvi.data.generator.ProblematicPositionsDataGenerator
import com.filipdolnik.mvi.domain.Player
import com.filipdolnik.mvi.util.parallelMap
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlin.system.measureTimeMillis

class DataGeneratorApplication: Application {

    override val description: String = "Generate new data"

    private val generators = listOf<Pair<DataGenerator, suspend (Player) -> DataSet>>(
        EndGameWithMovesDataGenerator() to { type -> DataSet.endGames(type) },
        FinalLoosingMovesDataGenerator() to { type -> DataSet.finalLoosingMoves(type) },
        FinalWinningMovesDataGenerator() to { type -> DataSet.finalWinningMoves(type) },
        FinalPositionsDataGenerator() to { type -> DataSet.finalPositions(type) },
        PreFinalPositionsDataGenerator() to { type -> DataSet.preFinalPositions(type) },
        ProblematicPositionsDataGenerator(Player.X, PersistedNeuralNetworkAi.classicalAi()) to
            { type -> DataSet.problematicPositions(type) },
        ProblematicPositionsDataGenerator(Player.O, PersistedNeuralNetworkAi.classicalAi()) to
            { type -> DataSet.problematicPositions(type) },
        GamesDataGenerator() to { type -> DataSet.games(type) },
        GamesWithOffsetDataGenerator() to { type -> DataSet.gamesWithOffset(type) },
        GamesWithPotentialMovesDataGenerator() to { type -> DataSet.gamesWithMoves(type) },
        GamesWithPotentialMovesDataGenerator() to { type -> DataSet.testing(type) }
    )

    private val maxIterations = 10

    override suspend fun start() {
        generators.forEach { (dataGenerator, dataSetFactory) ->
            for (i in 0 until maxIterations) {
                val playerXDataSet = dataSetFactory(Player.X)
                val playerODataSet = dataSetFactory(Player.O)

                val isPlayerXSupported = dataGenerator.supportedTypes.contains(Player.X)
                val isPlayerOSupported = dataGenerator.supportedTypes.contains(Player.O)

                val playerXRemainingDataCount = dataGenerator.targetDataSetSize - playerXDataSet.data.size
                val playerORemainingDataCount = dataGenerator.targetDataSetSize - playerODataSet.data.size
                if ((playerXRemainingDataCount <= 0 || !isPlayerXSupported) && (playerORemainingDataCount <= 0 || !isPlayerOSupported)) {
                    break
                }

                println("${playerXDataSet.name}: ")

                val time = measureTimeMillis {
                    val evaluatedPositionGroups = generateEvaluatedPositionGroups(dataGenerator)

                    lateinit var playerXData: List<PositionGroupWithEvaluation>
                    lateinit var playerOData: List<PositionGroupWithEvaluation>

                    coroutineScope {
                        if (isPlayerXSupported) {
                            launch(Dispatchers.Default) {
                                playerXData = evaluatedPositionGroups
                                    .filter { it.positionGroup.canonicalPosition.currentPlayer == Player.X }
                                    .take(playerXRemainingDataCount.coerceAtLeast(0))
                                playerXDataSet.addAll(playerXData)
                                playerXDataSet.save()
                            }
                        } else {
                            playerXData = emptyList()
                        }

                        if (isPlayerOSupported) {
                            launch(Dispatchers.Default) {
                                playerOData = evaluatedPositionGroups
                                    .filter { it.positionGroup.canonicalPosition.currentPlayer == Player.O }
                                    .take(playerORemainingDataCount.coerceAtLeast(0))
                                playerODataSet.addAll(playerOData)
                                playerODataSet.save()
                            }
                        } else {
                            playerOData = emptyList()
                        }
                    }

                    println("Player X -- new: ${playerXData.size}, remaining: ${playerXRemainingDataCount - playerXData.size}")
                    println("Player O -- new: ${playerOData.size}, remaining: ${playerORemainingDataCount - playerOData.size}")
                }

                println("Time: ${time / 1000.0}")
                println()
            }
        }
    }

    private suspend fun generateEvaluatedPositionGroups(dataGenerator: DataGenerator): List<PositionGroupWithEvaluation> {
        val generatedPositionGroups = dataGenerator.generate(dataGenerator.recommendedBatchSize)
        val deduplicatedPositionGroups = deduplicatePositionGroups(generatedPositionGroups)
        val evaluatedPositionGroups = evaluate(deduplicatedPositionGroups)

        println("Total -- generated: ${generatedPositionGroups.size}, new: ${evaluatedPositionGroups.size}")

        return evaluatedPositionGroups
    }

    private suspend fun deduplicatePositionGroups(newPositionGroups: List<PositionGroup>): List<PositionGroup> {
        var result = newPositionGroups.distinct()

        generators.forEach { (_, dataSetFactory) ->
            for (type in Player.values()) {
                val dataSet = dataSetFactory(type)

                val knownPositionGroups = dataSet.data.map { it.positionGroup }.toSet()

                result = result.filterNot { knownPositionGroups.contains(it) }
            }
        }

        return result
    }

    private suspend fun evaluate(positionGroups: List<PositionGroup>): List<PositionGroupWithEvaluation> = positionGroups.parallelMap {
        val ai = CurrentPositionAi()

        val evaluation = ai.evaluatePosition(it.canonicalPosition)

        PositionGroupWithEvaluation(it, evaluation.mergedEvaluation)
    }
}