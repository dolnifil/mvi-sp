package com.filipdolnik.mvi.app

import com.filipdolnik.mvi.ai.ml.NetworkBuilder
import com.filipdolnik.mvi.ai.ml.NeuralNetworkAi
import com.filipdolnik.mvi.ai.ml.PersistedNeuralNetworkAi
import com.filipdolnik.mvi.ai.ml.impl.ConvolutionalNetworkBuilder
import com.filipdolnik.mvi.ai.ml.impl.PositionDataSetIterator
import com.filipdolnik.mvi.data.DataSet
import com.filipdolnik.mvi.data.PositionGroupWithEvaluation
import com.filipdolnik.mvi.data.PositionWithEvaluation
import com.filipdolnik.mvi.domain.Player
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork
import org.deeplearning4j.optimize.listeners.PerformanceListener
import org.deeplearning4j.ui.api.UIServer
import org.deeplearning4j.ui.model.stats.StatsListener
import org.deeplearning4j.ui.model.storage.InMemoryStatsStorage
import org.nd4j.common.config.ND4JEnvironmentVars
import org.nd4j.evaluation.regression.RegressionEvaluation
import org.nd4j.linalg.activations.impl.ActivationSigmoid
import kotlin.system.measureTimeMillis

class ClassicalAiTrainingApplication: Application {

    override val description: String = "Classical Ai training"

    private val name = "classic"
    private val type = Player.X
    private val firstEpochIndex = 0

    private val batchSize = 1024
    private val epochs = 10
    private val networkBuilder = ConvolutionalNetworkBuilder()

    private suspend fun trainingDataSets(): List<PositionGroupWithEvaluation> =
        listOf(
            DataSet.games(type).data,
            DataSet.gamesWithOffset(type).data,
            DataSet.gamesWithMoves(type).data,
            DataSet.endGames(type).data,
            DataSet.finalLoosingMoves(type).data,
            DataSet.finalWinningMoves(type).data,
            DataSet.finalPositions(type).data,
            DataSet.preFinalPositions(type).data,
            DataSet.problematicPositions(type).data,
        )
            .flatten()

    private suspend fun testingDataSet(): List<PositionGroupWithEvaluation> = DataSet.testing(type).data

    override suspend fun start() {
        NeuralNetworkAi.enableDebug()

        val statsStorage = startUIServer()

        val model = loadModelOrCreateNew(networkBuilder, statsStorage)

        trainModel(model)
    }

    private fun startUIServer(): InMemoryStatsStorage {
        val uiServer = UIServer.getInstance()
        val statsStorage = InMemoryStatsStorage()

        uiServer.attach(statsStorage)

        return statsStorage
    }

    private fun loadModelOrCreateNew(networkBuilder: NetworkBuilder, statsStorage: InMemoryStatsStorage): MultiLayerNetwork {
        val file = PersistedNeuralNetworkAi.networkFile(name, type, firstEpochIndex - 1)
        val model = if (file.exists()) {
            MultiLayerNetwork.load(file, true)
        } else {
            networkBuilder.build()
        }

        println(model.summary())

        model.setListeners(StatsListener(statsStorage), PerformanceListener(100, true, true))

        return model
    }

    private suspend fun trainModel(model: MultiLayerNetwork) {
        val trainingDataSet = PositionDataSetIterator.forTraining(batchSize, trainingDataSets())
        val testingDataSet = PositionDataSetIterator.forTraining(batchSize, testingDataSet())

        val time = measureTimeMillis {
            repeat(epochs) {
                runTrainingEpoch(firstEpochIndex + it, model, trainingDataSet, testingDataSet)
            }
        }

        println("Time ${time / 1000.0}")
    }

    private suspend fun runTrainingEpoch(
        epochIndex: Int,
        model: MultiLayerNetwork,
        trainingDataSet: PositionDataSetIterator,
        testingDataSet: PositionDataSetIterator
    ) {
        model.fit(trainingDataSet)

        val testingEvaluation = model.evaluateRegression<RegressionEvaluation>(testingDataSet)

        save(model, epochIndex)

        println("Epoch $epochIndex ended. Testing evaluation:")
        println(testingEvaluation.stats())
    }

    private suspend fun save(model: MultiLayerNetwork, epochIndex: Int) {
        val file = PersistedNeuralNetworkAi.networkFile(name, type, epochIndex)

        withContext(Dispatchers.IO) {
            @Suppress("BlockingMethodInNonBlockingContext")
            model.save(file, true)
        }
    }
}