package com.filipdolnik.mvi.app

import com.filipdolnik.mvi.ai.ml.NeuralNetworkAi
import com.filipdolnik.mvi.ai.position.CurrentPositionAi
import com.filipdolnik.mvi.app.gamestate.AiDebugGameStateDescription
import com.filipdolnik.mvi.domain.Coordinates
import com.filipdolnik.mvi.domain.Game
import com.filipdolnik.mvi.view.GameStateView

class AiDebuggingApplication: Application {

    override val description: String = "Ai Debug"

    override suspend fun start() {
        NeuralNetworkAi.enableDebug()

        val ai = CurrentPositionAi()
        val game = Game()

        val playerXMoves = listOf(
            8 to 5,
            9 to 5,
            10 to 5,
            8 to 6,
            9 to 7,
            12 to 5,
            11 to 7,
            7 to 5,
        ).map { (x, y) -> Coordinates.fromValid(x, y) }

        val playerOMoves = listOf(
            11 to 5,
            11 to 9,
            8 to 4,
            9 to 6,
            10 to 6,
            11 to 6,
            10 to 4
        ).map { (x, y) -> Coordinates.fromValid(x, y) }

        for (i in playerXMoves.indices) {
            game.playNextMove(playerXMoves[i])

            if (i < playerOMoves.size) {
                game.playNextMove(playerOMoves[i])
            } else {
                break
            }
        }

        GameStateView().printGameState(AiDebugGameStateDescription(game, ai.evaluateAllNextMoves(game.position)))

        println(ai.evaluatePosition(game.position))
    }
}