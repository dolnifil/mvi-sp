package com.filipdolnik.mvi.app

import ch.qos.logback.classic.Level
import com.filipdolnik.mvi.ai.Ai
import com.filipdolnik.mvi.ai.ml.NeuralNetworkAi
import com.filipdolnik.mvi.app.gamestate.AiDebugGameStateDescription
import com.filipdolnik.mvi.app.gamestate.DefaultGameStateDescription
import com.filipdolnik.mvi.domain.Game
import com.filipdolnik.mvi.domain.Player
import com.filipdolnik.mvi.util.StopWatch
import com.filipdolnik.mvi.view.AiSelectionView
import com.filipdolnik.mvi.view.GameView

class AivAiInspectApplication: Application {

    override val description: String = "Ai vs Ai inspection"

    private val game: Game = Game()

    private val gameView = GameView()
    private var gameStateDescription = AiDebugGameStateDescription(game, emptyList())

    private lateinit var playerX: Ai
    private lateinit var playerO: Ai

    private val currentAi: Ai
        get() = when (game.currentPlayer) {
            Player.O -> playerO
            Player.X -> playerX
        }

    override suspend fun start() {
        NeuralNetworkAi.disableDebug()

        playerX = AiSelectionView.selectAi()()
        playerO = AiSelectionView.selectAi()()

        playGame()

        handleGameEnd()
    }

    private suspend fun playGame() {
        while (game.isNotEnded) {
            aiMoveStopWatch.measureTimeSuspending {
                updateGameState()
            }

            gameView.printGameState(gameStateDescription)

            playNextMove()

            StopWatch.printAllTimes()
            StopWatch.reset()
        }
    }

    private fun handleGameEnd() {
        gameView.printGameState(DefaultGameStateDescription(game))

        val gameResult = game.result
        if (gameResult != null) {
            gameView.printGameResult(gameResult, game.moveCount)
        } else {
            gameView.printGameEndedWithoutResult()
        }
    }

    private suspend fun playNextMove() {
        aiMoveStopWatch.measureTimeSuspending {
            val nextAiMoveCoordinates = currentAi.suggestNextMove(game.position)

            game.playNextMove(nextAiMoveCoordinates)
        }
    }

    private suspend fun updateGameState() {
        val nextMovesEvaluation = currentAi.evaluateAllNextMoves(game.position)

        gameStateDescription = AiDebugGameStateDescription(game, nextMovesEvaluation)
    }

    companion object {

        private val aiMoveStopWatch by lazy {
            StopWatch("Ai move")
        }
    }
}