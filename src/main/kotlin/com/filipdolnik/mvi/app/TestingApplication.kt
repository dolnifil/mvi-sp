package com.filipdolnik.mvi.app

import com.filipdolnik.mvi.ai.AiFactory
import com.filipdolnik.mvi.ai.minmax.MinMaxCurrentPositionAi
import com.filipdolnik.mvi.ai.position.CurrentPositionAi

class TestingApplication: BaseAivAiStatisticsApplication() {

    override val description: String = "Testing"

    override val playerXFactory: AiFactory = ::CurrentPositionAi

    override val playerOFactory: AiFactory = ::MinMaxCurrentPositionAi

    override val iterations: Int = 10
}