package com.filipdolnik.mvi.util

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext

suspend inline fun <T, R> Iterable<T>.parallelMap(
    context: CoroutineContext = Dispatchers.Default,
    crossinline transform: suspend (T) -> R
): List<R> = withContext(context) {
    map { async { transform(it) } }.awaitAll()
}