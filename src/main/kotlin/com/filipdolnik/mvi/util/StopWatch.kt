package com.filipdolnik.mvi.util

import java.util.concurrent.CopyOnWriteArrayList
import java.util.concurrent.atomic.AtomicLong
import kotlin.system.measureNanoTime

class StopWatch(val name: String) {

    val totalTime: AtomicLong = AtomicLong()
    val count: AtomicLong = AtomicLong()

    init {
        allStopWatches.add(this)
    }

    suspend inline fun <T> measureTimeSuspending(crossinline closure: suspend () -> T): T {
        val result: T
        val time = measureNanoTime { result = closure() }
        totalTime.addAndGet(time)
        count.incrementAndGet()
        return result
    }

    inline fun <T> measureTime(crossinline closure: () -> T): T {
        val result: T
        val time = measureNanoTime { result = closure() }
        totalTime.addAndGet(time)
        count.incrementAndGet()
        return result
    }

    companion object {

        private val allStopWatches = CopyOnWriteArrayList<StopWatch>()

        fun printAllTimes() {
            println("Times: ")
            allStopWatches.forEach {
                println("${it.name}: ${it.totalTime.get() / 1_000_000_000.0}; Called: ${it.count.get()}")
            }
        }

        fun reset() {
            allStopWatches.forEach {
                it.totalTime.set(0)
                it.count.set(0)
            }
        }
    }
}